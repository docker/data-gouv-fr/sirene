<?php

require_once (__DIR__."/../model.php");

/**
 * API Functions
 */

// Get one item (Association par son rna)
function _get($id)
{
	$data=array();

	// Données Association
	$db_start_time=microtime(true);

	$data['association'] = getAssociationByRNA(strtoupper($id));
	if ( $data['association'] === null  ) {

		return null;

	} else {

		$data['duration']['db'] = microtime(true) - $db_start_time;
		return $data;

	}


}

?>