<?php

	require_once (__DIR__."/../model.php");

	/**
	 * API Functions
	*/


// Get one item (Association par son Siret)
function _get($id)
{
	$data=array();

	// Données établissement
	$db_start_time=microtime(true);

	$data['association'] = getAssociationBySiret($id);

	if ( $data['association'] === null  ) {

		return null;

	} else {

		$data['duration']['db'] = microtime(true) - $db_start_time;
		return $data;

	}

}

?>