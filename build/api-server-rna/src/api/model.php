<?php

    // getAssociationByRNA
	function getAssociationByRNA($id)
	{
		$db = new DB();

		$data=array();

		// Données Assocaition
		$sql = "SELECT  *
		        FROM rna
				WHERE id_association = '$id'";

        // ==== Handle the request ====
		$data = $db->query($sql);

		if ( $db->rowcount == 0  ) {

			return null;

		} else {

            // Un seul recordset, on peut le retourner
			return $data;
		}

		unset ($db);

	}


    // getAssociationBySiret
	function getAssociationBySiret($id)
	{
		$db = new DB();

		$data=array();

		// Données Assocaition
		$sql = "SELECT  *
		        FROM rna
				WHERE siret = '$id'";

        // ==== Handle the request ====
		$data = $db->query($sql);

		if ( $db->rowcount == 0  ) {

			return null;

		} else {

            // Un seul recordset, on peut le retourner
			return $data;
		}

		unset ($db);

	}

	// Healthcheck
	function healthCheck()
	{
	    $data = dbHealthCheck();
	    return $data;
	}


    // dbHealthCheck : function added when db is populated
	function dbHealthCheck()
	{
		$db = new DB();

		$data=array();

		// Données établissement
		$sql = "SELECT *
		        FROM healthcheck();";

		// ==== Handle the request ====
		try {
            $data = $db->query($sql);
        } catch (PDOException $e) {
            return array(
                	"status" => "unhealthy",
                	"message" => "function healthcheck() not present in database"
                	);
        }

        // Un seul recordset, on peut le retourner
        $data["status"] = "healthy";
        return $data;

		unset ($db);

	}




?>