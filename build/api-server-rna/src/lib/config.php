<?php

    // la DB
    $db_conn = 'pgsql:';

    $db_name = isset($_ENV["POSTGRES_DB_NAME"]) ? $_ENV["POSTGRES_DB_NAME"] : "rna";
    $db_conn .= "dbname=" . $db_name;

    $db_host = isset($_ENV["POSTGRES_DB_HOST"]) ? $_ENV["POSTGRES_DB_HOST"] : "db";
    $db_conn .= ";host=" . $db_host;

    $db_port = isset($_ENV["POSTGRES_DB_PORT"]) ? $_ENV["POSTGRES_DB_PORT"] : "5432";
    $db_conn .= ";port=" . $db_port;

    define("DB_CONN",    $db_conn);
    define("DB_USER",    isset($_ENV["POSTGRES_DB_USER"])     ? $_ENV["POSTGRES_DB_USER"]     : "Missing POSTGRES_DB_USER envvar" );
    define("DB_PASS",    isset($_ENV["POSTGRES_DB_PASSWORD"]) ? $_ENV["POSTGRES_DB_PASSWORD"] : "Missing POSTGRES_DB_PASSWORD envvar" );

?>
