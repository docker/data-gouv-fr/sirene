#!/bin/bash

export PGUSER=${POSTGRES_USER:-sirene_user}
export PGDATABASE=${POSTGRES_DB:-sirene_db}

psql -U ${PGUSER} -d ${PGDATABASE} -c "CREATE EXTENSION pg_trgm;"
psql -U ${PGUSER} -d ${PGDATABASE} -c "ALTER SYSTEM SET max_wal_size = '10GB';"
psql -U ${PGUSER} -d ${PGDATABASE} -c "ALTER SYSTEM SET archive_mode = 'off';"
psql -U ${PGUSER} -d ${PGDATABASE} -c "ALTER SYSTEM SET work_mem = '2GB';"
psql -U ${PGUSER} -d ${PGDATABASE} -c "SELECT * FROM pg_reload_conf();"


# Permissions pour l'utilisateur web_anon
echo "Création dsur les tables:"
psql -U ${PGUSER} -d ${PGDATABASE} -c "create role web_anon nologin;"

psql -U ${PGUSER} -d ${PGDATABASE} -c "
grant usage on schema public to web_anon;
grant all on all tables in schema public to web_anon;
grant web_anon to ${PGUSER};
"



echo "Event Trigger (pour refresh schema auto)"
psql -U ${PGUSER} -d ${PGDATABASE} -c'
CREATE OR REPLACE FUNCTION public.notify_ddl_postgrest()
  RETURNS event_trigger
 LANGUAGE plpgsql
  AS $$
BEGIN
  NOTIFY ddl_command_end;
END;
$$;
'

psql -U ${PGUSER} -d ${PGDATABASE} -c '
CREATE EVENT TRIGGER ddl_postgrest ON ddl_command_end
   EXECUTE PROCEDURE public.notify_ddl_postgrest();
'
