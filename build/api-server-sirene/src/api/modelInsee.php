<?php

    require_once (__DIR__."/modelSpool.php");

    // getEtablissement --> format Insee
	function getInseeSiret($id, $siege_bool = false)
	{
		$db = new DB();

		$data=array();

        // Données établissement
		$sql = "SELECT  id,
                        siren,
                        nic,
                        siret,
                        statut_diffusion,
                        date_creation,
                        tranche_effectifs,
                        annee_effectifs,
                        activite_principale_registre_metiers,
                        date_dernier_traitement,
                        etablissement_siege,
                        nombre_periodes,

                        complement_adresse,
                        numero_voie,
                        indice_repetition,
                        type_voie,
                        libelle_voie,
                        code_postal,
                        libelle_commune,
                        libelle_commune_etranger,
                        distribution_speciale,
                        code_commune,
                        code_cedex,
                        libelle_cedex,
                        code_pays_etranger,
                        libelle_pays_etranger,

                        complement_adresse_2,
                        numero_voie_2,
                        indice_repetition_2,
                        type_voie_2,
                        libelle_voie_2,
                        code_postal_2,
                        libelle_commune_2,
                        libelle_commune_etranger_2,
                        distribution_speciale_2,
                        code_commune_2,
                        code_cedex_2,
                        libelle_cedex_2,
                        code_pays_etranger_2,
                        libelle_pays_etranger_2,
                        date_debut,
                        etat_administratif,
                        enseigne_1,
                        enseigne_2,
                        enseigne_3,
                        denomination_usuelle,
                        activite_principale,
                        nomenclature_activite_principale,
                        caractere_employeur ,
                        longitude,
                        latitude,
                        geo_score,
                        geo_type,
                        geo_adresse,
                        geo_id,
                        geo_ligne,
                        geo_l4,
                        geo_l5,
                        created_at,
                        updated_at
		        FROM etablissement
				WHERE siret = '$id'";

				// Siege ?
				if ($siege_bool === true) {
                       $sql .= " AND etablissement_siege is true";
				}


		// ==== Handle the request ====
		$data = $db->query($sql);

		if ( $db->rowcount == 0  ) {

            $res = $db->query("INSERT INTO spool (item_type, item_id) VALUES ('siren', '" . substr($id,0,9) . "');");
            $res = $db->query("INSERT INTO spool (item_type, item_id) VALUES ('siret', '$id');");
			return null;

		} else {

            // Un seul recordset, à remettre en forme
			$etablissement = array(

                "siren"     => $data['siren'],
                "nic"       => $data['nic'],
                "siret"     => $data['siren'],
                "statutDiffusionEtablissement"                      => $data['statut_diffusion'],
                "dateCreationEtablissement"                         => $data['date_creation'],
                "trancheEffectifsEtablissement"                     => $data['tranche_effectifs'],
                "anneeEffectifsEtablissement"                       => $data['annee_effectifs'],
                "activitePrincipaleRegistreMetiersEtablissement"    => $data['activite_principale_registre_metiers'],
                "dateDernierTraitementEtablissement"                => $data['date_dernier_traitement'],
                "etablissementSiege"                                => $data['etablissement_siege'],
                "nombrePeriodesEtablissement"                       => $data['nombre_periodes']

            );

            // Adresse
            $etablissement['adresseEtablissement'] = array (
                "complementAdresseEtablissement"           => $data['complement_adresse'],
                "numeroVoieEtablissement"                  => $data['numero_voie'],
                "indiceRepetitionEtablissement"            => $data['indice_repetition'],
                "typeVoieEtablissement"                    => $data['type_voie'],
                "libelleVoieEtablissement"                 => $data['libelle_voie'],
                "codePostalEtablissement"                  => $data['code_postal'],
                "libelleCommuneEtablissement"              => $data['libelle_commune'],
                "libelleCommuneEtrangerEtablissement"      => $data['libelle_commune_etranger'],
                "distributionSpecialeEtablissement"        => $data['distribution_speciale'],
                "codeCommuneEtablissement"                 => $data['code_commune'],
                "codeCedexEtablissement"                   => $data['code_cedex'],
                "libelleCedexEtablissement"                => $data['libelle_cedex'],
                "codePaysEtrangerEtablissement"            => $data['code_pays_etranger'],
                "libellePaysEtrangerEtablissement"         => $data['libelle_pays_etranger']
            );


            // // Adresse2 --> Désactivé par API Insee version 3.11 (2024.03.22)
            // $etablissement['adresse2Etablissement'] = array (
            //     "complementAdresse2Etablissement"           => $data['complement_adresse_2'],
            //     "numeroVoie2Etablissement"                  => $data['numero_voie_2'],
            //     "indiceRepetition2Etablissement"            => $data['indice_repetition_2'],
            //     "typeVoie2Etablissement"                    => $data['type_voie_2'],
            //     "libelleVoie2Etablissement"                 => $data['libelle_voie_2'],
            //     "codePostal2Etablissement"                  => $data['code_postal_2'],
            //     "libelleCommune2Etablissement"              => $data['libelle_commune_2'],
            //     "libelleCommuneEtranger2Etablissement"      => $data['libelle_commune_etranger_2'],
            //     "distributionSpeciale2Etablissement"        => $data['distribution_speciale_2'],
            //     "codeCommune2Etablissement"                 => $data['code_commune_2'],
            //     "codeCedex2Etablissement"                   => $data['code_cedex_2'],
            //     "libelleCedex2Etablissement"                => $data['libelle_cedex_2'],
            //     "codePaysEtranger2Etablissement"            => $data['code_pays_etranger_2'],
            //     "libellePaysEtranger2Etablissement"         => $data['libelle_pays_etranger_2']
            // );

            // Périodes
            $etablissement['periodesEtablissement'] = array (
                "000-a_coder_si_necessaire"  => "Tableau de " . $data['nombre_periodes'] . " périodes"
            );

            unset($data);
            return $etablissement;

		}

		unset ($db);

	}


    // getUniteLegale => format Insee pour intégration dans /siret
	function getInseeUniteLegale($id)
	{
		$db = new DB();

		$data=array();

		// L'Unité légale correspondante
		$sql = "SELECT activite_principale,
                       annee_categorie_entreprise,
                       annee_effectifs,
                       caractere_employeur,
                       categorie_entreprise,
                       categorie_juridique,
                       created_at,
                       date_creation,
                       date_debut,
                       date_dernier_traitement,
                       date_fin,
                       denomination,
                       denomination_usuelle_1,
                       denomination_usuelle_2,
                       denomination_usuelle_3,
                       economie_sociale_solidaire,
                       etat_administratif,
                       id,
                       identifiant_association,
                       nic_siege,
                       nom,
                       nom_usage,
                       nombre_etablissements,
                       nombre_periodes,
                       nomenclature_activite_principale,
                       numero_tva_intra,
                       prenom_1,
                       prenom_2,
                       prenom_3,
                       prenom_4,
                       prenom_usuel,
                       pseudonyme,
                       sexe,
                       sigle,
                       siren,
                       statut_diffusion,
                       tranche_effectifs,
                       unite_purgee,
                       updated_at";

		$sql.= " FROM unite_legale WHERE siren= '" . $id . "'";

		// ==== Handle the request ====
		$data = $db->query($sql);

		if ( $db->rowcount == 0  ) {

            $res = $db->query("INSERT INTO spool (item_type, item_id) VALUES ('siren', '$id');");
			return null;

		} else {

            // Un seul recordset, à remettre en forme
			$uniteLegale = array(

                "etatAdministratifUniteLegale"                  => $data['etat_administratif'],
                "statutDiffusionUniteLegale"                    => $data['statut_diffusion'],
                "dateCreationUniteLegale"                       => $data['date_creation'],
                "categorieJuridiqueUniteLegale"                 => $data['categorie_juridique'],
                "denominationUniteLegale"                       => $data['denomination'],
                "sigleUniteLegale"                              => $data['sigle'],
                "denominationUsuelle1UniteLegale"               => $data['denomination_usuelle_1'],
                "denominationUsuelle2UniteLegale"               => $data['denomination_usuelle_2'],
                "denominationUsuelle3UniteLegale"               => $data['denomination_usuelle_3'],
                "sexeUniteLegale"                               => $data['sexe'],
                "nomUniteLegale"                                => $data['nom'],
                "nomUsageUniteLegale"                           => $data['nom_usage'],
                "prenom1UniteLegale"                            => $data['prenom_1'],
                "prenom2UniteLegale"                            => $data['prenom_2'],
                "prenom3UniteLegale"                            => $data['prenom_3'],
                "prenom4UniteLegale"                            => $data['prenom_4'],
                "prenomUsuelUniteLegale"                        => $data['prenom_usuel'],
                "pseudonymeUniteLegale"                         => $data['pseudonyme'],
                "activitePrincipaleUniteLegale"                 => $data['activite_principale'],
                "nomenclatureActivitePrincipaleUniteLegale"     => $data['nomenclature_activite_principale'],
                "identifiantAssociationUniteLegale"             => $data['identifiant_association'],
                "economieSocialeSolidaireUniteLegale"           => $data['economie_sociale_solidaire'],
                "caractereEmployeurUniteLegale"                 => $data['caractere_employeur'],
                "trancheEffectifsUniteLegale"                   => $data['tranche_effectifs'],
                "anneeEffectifsUniteLegale"                     => $data['annee_effectifs'],
                "nicSiegeUniteLegale"                           => $data['nic_siege'],
                "dateDernierTraitementUniteLegale"              => $data['date_dernier_traitement'],
                "categorieEntreprise"                           => $data['categorie_entreprise'],
                "anneeCategorieEntreprise"                      => $data['annee_categorie_entreprise']

            );

            unset($data);
            return $uniteLegale;
		}

		unset ($db);

	}


    // getUniteLegale => format /siren de l'Insee
	function getInseeSiren($id, $periodes = false)
	{
		$db = new DB();

		$data=array();

		// L'Unité légale correspondante
		$sql = "SELECT activite_principale,
                       annee_categorie_entreprise,
                       annee_effectifs,
                       caractere_employeur,
                       categorie_entreprise,
                       categorie_juridique,
                       created_at,
                       date_creation,
                       date_debut,
                       date_dernier_traitement,
                       date_fin,
                       denomination,
                       denomination_usuelle_1,
                       denomination_usuelle_2,
                       denomination_usuelle_3,
                       economie_sociale_solidaire,
                       etat_administratif,
                       id,
                       identifiant_association,
                       nic_siege,
                       nom,
                       nom_usage,
                       nombre_etablissements,
                       nombre_periodes,
                       nomenclature_activite_principale,
                       numero_tva_intra,
                       prenom_1,
                       prenom_2,
                       prenom_3,
                       prenom_4,
                       prenom_usuel,
                       pseudonyme,
                       sexe,
                       sigle,
                       siren,
                       statut_diffusion,
                       tranche_effectifs,
                       unite_purgee,
                       updated_at";

		$sql.= " FROM unite_legale WHERE siren= '" . $id . "'";

		// ==== Handle the request ====
		$data = $db->query($sql);

		if ( $db->rowcount == 0  ) {

            $res = $db->query("INSERT INTO spool (item_type, item_id) VALUES ('siren', '$id');");
			return null;

		} else {

            // Un seul recordset, à remettre en forme
			$uniteLegale = array(
                "siren"                                         => $data['siren'],
                "statutDiffusionUniteLegale"                    => $data['statut_diffusion'],
                "dateCreationUniteLegale"                       => $data['date_creation'],
                "sigleUniteLegale"                              => $data['sigle'],
                "sexeUniteLegale"                               => $data['sexe'],
                "prenom1UniteLegale"                            => $data['prenom_1'],
                "prenom2UniteLegale"                            => $data['prenom_2'],
                "prenom3UniteLegale"                            => $data['prenom_3'],
                "prenom4UniteLegale"                            => $data['prenom_4'],
                "prenomUsuelUniteLegale"                        => $data['prenom_usuel'],
                "pseudonymeUniteLegale"                         => $data['pseudonyme'],
                "identifiantAssociationUniteLegale"             => $data['identifiant_association'],
                "trancheEffectifsUniteLegale"                   => $data['tranche_effectifs'],
                "anneeEffectifsUniteLegale"                     => $data['annee_effectifs'],
                "dateDernierTraitementUniteLegale"              => $data['date_dernier_traitement'],
                "nombrePeriodesUniteLegale"                     => $data['nombre_periodes'],
                "categorieEntreprise"                           => $data['categorie_entreprise'],
                "anneeCategorieEntreprise"                      => $data['annee_categorie_entreprise']

            );

/*   Champs Historisés (périodes)
                "etatAdministratifUniteLegale"                  => $data['etat_administratif'],
                "categorieJuridiqueUniteLegale"                 => $data['categorie_juridique'],
                "denominationUniteLegale"                       => $data['denomination'],
                "denominationUsuelle1UniteLegale"               => $data['denomination_usuelle_1'],
                "denominationUsuelle2UniteLegale"               => $data['denomination_usuelle_2'],
                "denominationUsuelle3UniteLegale"               => $data['denomination_usuelle_3'],
                "nomUniteLegale"                                => $data['nom'],
                "nomUsageUniteLegale"                           => $data['nom_usage'],
                "activitePrincipaleUniteLegale"                 => $data['activite_principale'],
                "nomenclatureActivitePrincipaleUniteLegale"     => $data['nomenclature_activite_principale'],
                "economieSocialeSolidaireUniteLegale"           => $data['economie_sociale_solidaire'],
                "caractereEmployeurUniteLegale"                 => $data['caractere_employeur'],
                "nicSiegeUniteLegale"                           => $data['nic_siege'],
*/

            unset($data);
            return $uniteLegale;
		}

		unset ($db);

	}


    // getUniteLegale => format /siren de l'Insee
	function getInseePeriodesUniteLegale($id)
	{
        return array("000-a-coder si necesssaire" => "avec table Valeurs historisées");
    }

?>