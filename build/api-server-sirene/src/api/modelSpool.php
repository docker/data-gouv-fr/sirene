<?php

    // addSpoolItem (un seul)
	function addSpoolItem($type, $id, $reason = "no reason")
    {

        // Need an item...
        if ($id === null) {
            return false;
        }

        $db = new DB();

   		$sql = "DELETE FROM spool where item_type= '$type' and item_id = '$id'; ";
        $db->query($sql);

        $sql = "INSERT INTO spool (item_type, item_id, reason) VALUES ('$type', '$id', '$reason')";
        $db->query($sql);

        unset ($db);

    }

    // getSpoolItem (un seul)
	function getSpoolItem($id)
	{
		$db = new DB();

		$data=array();

        // Données établissement
		$sql = "SELECT  *
		        FROM spool
				WHERE item_id = '$id'";


		// ==== Handle the request ====
		$data = $db->query($sql);

		if ( $db->rowcount == 0  ) {

			return false;

		} else {

            // Un seul recordset, on peut le retourner
			return $data;
		}

		unset ($db);

	}

    /*
        Supprime un élément du spool
    */
    function rmSpoolItem($id = null)
    {

        // Need an item...
        if ($id === null) {
            return false;
        }

        $db = new DB();

		$sql = "DELETE
                FROM spool";

        if ( $id !== "ALL" ) {
            $sql .= " WHERE item_id = '$id'";
        }

        // ==== Handle the request ====
		$db->query($sql);

        unset ($db);


    }


    // getSpoolItems
	function getSpoolItems()
	{

		$db = new DB();

		$data=array();

        // On récupère juste le numéro de siren par une recherche multi-critère
        $sql = "SELECT *
        FROM   spool
        ";

        $sql.= " LIMIT " . MAX_ITEMS_PER_QUERY;

        // ==== Handle the request ====
		try {
            $data = $db->query($sql);
        } catch (PDOException $e) { return false; }

		switch ($db->rowcount)  {

          case 0:
			return false;

          case 1:  // On retourne la ligne (dans un tableau)
            return array ($data);

          default: // On retourne les lignes (c'est déjà un tableau)
			return $data;
		}

		unset ($db);

	}

?>