<?php


	require_once (__DIR__."/../model.php");

	/*
	* API Functions
	*/

    // Get one item (UniteLegale)

	function _get($siren = null, $reqparam = null)
	{

		$db_start_time=microtime(true);

		// On a un ID
		// - un id (aucun autre caractère que numérique)
		// - on ignore "reqparam", qui pourrait servir de filtre complémentaire
	
		if (  $siren !== null   && strlen($siren) == 9   && ctype_digit($siren)  ) {

			 // on nous donné juste un mot, on suppose que c'est un SIREN
			// Données unité légale
			$data=array();
			$data['unite_legale'] = getUniteLegaleEtEtablissements($siren);

			if ( $data['unite_legale'] === null ) {

				return 404;

			} else {

				$data['duration']['db'] = microtime(true) - $db_start_time;
				return $data;

			}
		}
		else
		{
			return 415;
		}

	}



	// Recherche multi-critères, pas de Siren fourni, juste des paramètres
	function _getAll($reqparam = null)
	{

		$db_start_time=microtime(true);

		// Analyse de $reqparam, qui peut être :
		// - un id (aucun autre caractère que numérique)
		// - un critère de recherche au format critere=valeur
		if ( is_array($reqparam) ) {

			// Le critère sert à récupérer un numéro de siren.
			$unitesLegales = getUnitesLegalesByParam($reqparam);

			if ( $unitesLegales === null ) {
				return 404;
			}
			else
			{

				$data=array();
				$data['unites_legales'] = array();

				// Pour chacune, on recup toutes les infos
				foreach ($unitesLegales as $ul)
				{
					array_push($data['unites_legales'], getUniteLegaleEtEtablissements($ul['siren']));
				}

				$data['duration']['db'] = microtime(true) - $db_start_time;
				return $data;


			}

		}

		else // Pas de paramètre, comment peut-on en arriver là ?
		{

				return 415;
		}


	}

	// PUT (Unité Légale) : demande de mise à jour
	function _put($siren = null)
	{

		if (  $siren !== null   && strlen($siren) == 9   && ctype_digit($siren)  ) {

			addSpoolItem("siren", $siren, "PUT called");
			return array("message" => "Mise à jour demandée pour unite_legale " . $siren);

		} else {
			return 415;
		}

	}

	// putAll : demande à raffraichir les siret depuis une date donnée
	/**
	 * On joue sur la colonne date_dernier_traitement de la table status. 
	 * que l'on positionne à celle spécifiée.
	 */
	function _putAll($params = null)
	{

		if (  $params !== null   && is_array($params)  ) {
			if (array_key_exists("ddt",$params) && validateDate($params["ddt"]) ) {

				resetDDT("unite_legale", $params["ddt"]);

			}

		} else {
			return 415;
		}

	}
?>