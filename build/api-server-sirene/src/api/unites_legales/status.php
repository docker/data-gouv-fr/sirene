<?php


		$endpoint_status=array(
			"title" => "endpoint /unites_legales",
			"definition" => "Teste la présence d'unites légales dans la vue 'unite_legale'",
			"vital" => true,
			"section" => 0,
			"level" => 1,
			"status" => "OK"
		);

		// Contrôle accès à la DB
		$db = new DB();

		try {
			$rs = $db->query("SELECT siren FROM unite_legale LIMIT 1");

			if ( ( $rs == false ) || (count($rs) == 0) ) {

					$endpoint_status["status"] = "KO";

			}

		} catch (PDOException $e) {
			$endpoint_status["status"]="KO";
		}


?>