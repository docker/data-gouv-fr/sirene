<?php

     require_once (__DIR__."/../modelInsee.php");

     /*
	 * API Functions
	 */

	// Référence : https://api.insee.fr/entreprises/sirene/VXXX/siret/43907415400048
	// Dev/Test : url -X GET -Ls http://127.0.0.1/api/sirene/vXXX/siret/43907415400048

    // Get one item (siret + UniteLegale + Siege)
	function _get($siren)
	{
		$data=array();

		if (  $siren !== null   && strlen($siren) == 9   && ctype_digit($siren)  ) {

			// Données établissement + adresse + périodes
			$data['uniteLegale'] = getInseeSiren($siren);

			if ( $data['uniteLegale'] === null  ) {

				return 404;

			} else {

				$db_start_time=microtime(true);

				// Ajout des périodes de l'unité légale
				$data['uniteLegale']['periodesUniteLegale'] = getInseePeriodesUniteLegale($siren);

				$data['duration']['db'] = microtime(true) - $db_start_time;
				return $data;

			}

		} else {
			return 415;
		}

	}

	// PUT (Unité Légale) : demande de mise à jour
	function _put($siren = null)
	{

		if (  $siren !== null   && strlen($siren) == 9   && ctype_digit($siren)  ) {

			addSpoolItem("siren", $siren, "PUT called");
			return array("message" => "Mise à jour demandée pour siren " . $siren);

		} else {
			return 415;
		}

	}

?>