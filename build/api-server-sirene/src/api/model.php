<?php

    require_once (__DIR__."/modelSpool.php");

    /**
     * validation des dates utilisée par /unites_legales et /etablissements
     */
    function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    // remise à une date arbitraire de maxLastUpdate pour siret, siten, ou succession
	function resetDDT($type, $date = null)
    {

        $db = new DB();

        $sql = "UPDATE status
                SET    date_dernier_traitement = '" . $date . "'
                WHERE  table_name  = '" . $type . "';";

        // On exécute cette requête.
        $db->query($sql);

        unset ($db);

    }


    // Remplace toutes les valeurs à "[ND]" par des null dans le tableau passé en param.
    function checkND($classe, $id, $data)
    {
        // Il faut afficher les champs "[ND]" seulement si le statut_diffusion est n'est pas "O"
        // Dans ce cas, on ne touche pas aux données, on force juste une vérification de non changement de statut

        // On a eu le cas d'une association pour laquelle il n'y avait pas de statut_diffusion
        if ( array_key_exists("statut_diffusion", $data) && $data["statut_diffusion"] != "O" ) {
            addSpoolItem($classe, $id, "Statut-Diff: " . $data["statut_diffusion"]);
            return $data;
        }

        // Si on est encore là, on checke les ND
        $changes=0;
        foreach ($data as $k => $v ) {
            if ( ! is_array($v) ) {
                if ( $v === "[ND]" ) {
                    $data[$k] = null;
                    $changes++;
                }
            } 
            else {
                // TODO. On a un tableau dans le tableau
            }
        }

        if ($changes > 0 ) {
            addSpoolItem($classe, $id, "Statut-Diff: " . $changes);
        }

        return $data;

        // $arr = array_replace($data, array_fill_keys( array_keys($data, "[ND]"), null ) );
        // return $arr;
    }

    // getUnitesLegalesByParam by une autre colonne arbitraire
    /**
      * ==> un tableau de siren ou null si pas de retour.
      */

	function getUnitesLegalesByParam($reqparam)
	{

		$db = new DB();

		$data=array();

        // On récupère juste le numéro de siren par une recherche multi-critère
        $sql = "SELECT siren
        FROM   unite_legale
        WHERE  siren is not null";

        // Ajout des critères
        // Seulement ceux listés
        // ==> Penser à l'index...
        foreach ($reqparam as $column=>$value) {

            switch ($column) {
                // Toutes celles valides doivent être citées ici
                case "siren":
                case "siret":
                case "identifiant_association":
                case "categorie_juridique":
                        $sql .= " AND $column = '$value' ";
                    break;

                // La requête multi-critère contient un critère qu'on refuse --> not found.
                default:
                    return false;
                    break;
            }
        }

        $sql.= " LIMIT " . MAX_ITEMS_PER_QUERY;

        // ==== Handle the request ====
		try {
            $data = $db->query($sql);
        } catch (PDOException $e) { return false; }

		switch ($db->rowcount)  {

          case 0:
			return null;

          case 1:  // On retourne la ligne (dans un tableau)
            return array ($data);

          default: // On retourne les lignes (c'est déjà un tableau)
			return $data;
		}

		unset ($db);

	}


    // getEtablissementsByParam by une colonne arbitraire
	function getEtablissementsByParam($reqparam)
	{

		$db = new DB();

		$data=array();

		$sql = "SELECT siret
		        FROM   etablissement
                WHERE  siret is not null";

        // Ajout des critères
        // Seulement ceux listés
        // ==> Penser à l'index...
        foreach ($reqparam as $column=>$value) {

            switch ($column) {
                // Toutes celles valides doivent être citées ici
                case "siren":
                case "siret":
                case "etat_administratif":
                        $sql .= " AND $column = '$value' ";
                    break;

                // La requête multi-critère contient un critère qu'on refuse --> not found.
                default:
                    return false;
                    break;
            }
        }

        $sql.= " LIMIT " . MAX_ITEMS_PER_QUERY;

        // ==== Handle the request ====
		try {
            $data = $db->query($sql);
        } catch (PDOException $e) { return false; }

		switch ($db->rowcount)  {

            case 0:
              return null;

            case 1:  // On retourne la ligne (dans un tableau)
              return array ($data);

            default: // On retourne les lignes (c'est déjà un tableau)
              return $data;
          }

          unset ($db);

		unset ($db);

	}


    // getEtablissement by ID
	function getEtablissement($id, $siege_bool = false)
	{
		$db = new DB();

		$data=array();

        // Données établissement
		$sql = "SELECT  id,
                        siren,
                        nic,
                        siret,
                        statut_diffusion,
                        date_creation,
                        tranche_effectifs,
                        annee_effectifs,
                        activite_principale_registre_metiers,
                        date_dernier_traitement,
                        etablissement_siege,
                        nombre_periodes,
                        complement_adresse,
                        numero_voie,
                        indice_repetition,
                        type_voie,
                        libelle_voie,
                        code_postal,
                        libelle_commune,
                        libelle_commune_etranger,
                        distribution_speciale,
                        code_commune,
                        code_cedex,
                        libelle_cedex,
                        code_pays_etranger,
                        libelle_pays_etranger,
                        complement_adresse_2,
                        numero_voie_2,
                        indice_repetition_2,
                        type_voie_2,
                        libelle_voie_2,
                        code_postal_2,
                        libelle_commune_2,
                        libelle_commune_etranger_2,
                        distribution_speciale_2,
                        code_commune_2,
                        code_cedex_2,
                        libelle_cedex_2,
                        code_pays_etranger_2,
                        libelle_pays_etranger_2,
                        date_debut,
                        etat_administratif,
                        enseigne_1,
                        enseigne_2,
                        enseigne_3,
                        denomination_usuelle,
                        activite_principale,
                        nomenclature_activite_principale,
                        caractere_employeur ,
                        longitude,
                        latitude,
                        geo_score,
                        geo_type,
                        geo_adresse,
                        geo_id,
                        geo_ligne,
                        geo_l4,
                        geo_l5,
                        created_at,
                        updated_at
		        FROM etablissement
				WHERE siret = '$id'";

				// Siege ?
				if ($siege_bool === true) {
                       $sql .= " AND etablissement_siege is true";
				}


		// ==== Handle the request ====
		$data = $db->query($sql);

		if ( $db->rowcount == 0  ) {

            addSpoolItem("siren", substr($id,0,9), "Not Found");
            addSpoolItem("siret", $id, "Not Found");
			return null;

		} else {

            // Si satut diffusion ND ou P : On loggue dans le spooler pour raffraichir
            // en cas de changement récent de statut_diffusion
            if ( array_key_exists("statut_diffusion", $data) && $data["statut_diffusion"] != "O" ) {

                addSpoolItem("siret", $id, "Statut-Diff: " . $data["statut_diffusion"]);

                if ( true === DISABLE_NON_DIFFUSIBLES ) return false;
            }

            // Un seul recordset, on peut le retourner, mais (depuis 2024.06.07) on le nettoie des éventuelles mentions "[ND]"
			return checkND("siret", $id, $data);
		}

		unset ($db);

	}


    // getEtablissements (tous ceux d'une unité légale)
	function getEtablissements($siren)
	{
		$db = new DB();

		$data=array();

		// Données établissements
		$sql = "SELECT  id,
                        siren,
                        nic,
                        siret,
                        statut_diffusion,
                        date_creation,
                        tranche_effectifs,
                        annee_effectifs,
                        activite_principale_registre_metiers,
                        date_dernier_traitement,
                        etablissement_siege,
                        nombre_periodes,
                        complement_adresse,
                        numero_voie,
                        indice_repetition,
                        type_voie,
                        libelle_voie,
                        code_postal,
                        libelle_commune,
                        libelle_commune_etranger,
                        distribution_speciale,
                        code_commune,
                        code_cedex,
                        libelle_cedex,
                        code_pays_etranger,
                        libelle_pays_etranger,
                        complement_adresse_2,
                        numero_voie_2,
                        indice_repetition_2,
                        type_voie_2,
                        libelle_voie_2,
                        code_postal_2,
                        libelle_commune_2,
                        libelle_commune_etranger_2,
                        distribution_speciale_2,
                        code_commune_2,
                        code_cedex_2,
                        libelle_cedex_2,
                        code_pays_etranger_2,
                        libelle_pays_etranger_2,
                        date_debut,
                        etat_administratif,
                        enseigne_1,
                        enseigne_2,
                        enseigne_3,
                        denomination_usuelle,
                        activite_principale,
                        nomenclature_activite_principale,
                        caractere_employeur ,
                        longitude,
                        latitude,
                        geo_score,
                        geo_type,
                        geo_adresse,
                        geo_id,
                        geo_ligne,
                        geo_l4,
                        geo_l5,
                        created_at,
                        updated_at
		        FROM etablissement
				WHERE siren = '$siren'
                ORDER BY etat_administratif, date_debut DESC";

		// ==== Handle the request ====
		$res = $db->query($sql);

        switch ($db->rowcount) {
            case 0:
                $data = null;
                break;

            case 1: // Singleton, passage en premier élément d'un tableau
                $data = checkND("siret", $res["siret"], array($res));
                break;

            default:
                // Appel de checkND pour chaque etablissement retourné.
                $data=array();
                foreach($res as $etablissement) {
                    array_push($data, checkND("siret", $etablissement["siret"], $etablissement));
                }
                break;
        }

        unset ($res);
        unset ($db);
        return $data;

	}


    // getUniteLegale
	function getUniteLegale($id, $etablissements = false)
	{
		$db = new DB();
		$data=array();

		// L'Unité légale correspondante
		$sql = "SELECT activite_principale,
                       annee_categorie_entreprise,
                       annee_effectifs,
                       caractere_employeur,
                       categorie_entreprise,
                       categorie_juridique,
                       created_at,
                       date_creation,
                       date_debut,
                       date_dernier_traitement,
                       date_fin,
                       denomination,
                       denomination_usuelle_1,
                       denomination_usuelle_2,
                       denomination_usuelle_3,
                       economie_sociale_solidaire,
                       etat_administratif,
                       id,
                       identifiant_association,
                       nic_siege,
                       nom,
                       nom_usage,
                       nombre_etablissements,
                       nombre_periodes,
                       nomenclature_activite_principale,
                       numero_tva_intra,
                       prenom_1,
                       prenom_2,
                       prenom_3,
                       prenom_4,
                       prenom_usuel,
                       pseudonyme,
                       sexe,
                       sigle,
                       siren,
                       societe_mission,
                       statut_diffusion,
                       tranche_effectifs,
                       unite_purgee,
                       updated_at";
		if ($etablissements === true) {
			$sql .= ", etablissements";
		}
		$sql.= " FROM unite_legale WHERE siren= '" . $id . "'";

		// ==== Handle the request ====
		$data = $db->query($sql);

		if ( $db->rowcount == 0  ) {

            addSpoolItem("siren", $id, "Not Found");
            return null;

		} else {

            // Si satut diffusion ND ou P : On loggue dans le spooler pour raffraichir
            // en cas de changement récent de statut_diffusion
            if ( array_key_exists("statut_diffusion", $data) && $data["statut_diffusion"] != "O" ) {

                addSpoolItem("siren", $id, "Statut-Diff: " . $data["statut_diffusion"]);

                if ( true === DISABLE_NON_DIFFUSIBLES ) return false;
            }

            // Un seul recordset, on peut le retourner, mais (depuis 2024.06.07) on le nettoie des éventuelles mentions "[ND]"
			return checkND("siren", $id, $data);

		}

		unset ($db);

	}


    // getUniteLegaleEtEtablissements (by Siren)
	function getUniteLegaleEtEtablissements($id)
    {

        // Données unité légale
        $ul=getUniteLegale($id);

        if ( $ul !== null ) {

            // Les établissements de cette UL
            $ul['etablissements']      = getEtablissements($ul['siren']);

            // L'établissement siège de cette UL
            $ul['etablissement_siege'] = getEtablissement($ul['siren'].$ul['nic_siege']);

            // Enrchissements pour coller à l'API data.gouv.fr : unite_legale_id
            if ( null !== $ul['etablissements'] ) {
                foreach (array_keys($ul['etablissements']) as $k ) {

                    if ( is_array($ul['etablissements'][$k]) ){
                        // if (array_key_exists('unite_legale_id', $ul['etablissements'][$k]) ){
                            $ul['etablissements'][$k]['unite_legale_id'] = $ul['id'];
                        // }
                    }

                }
            }

            if ( null !== $ul['etablissement_siege'] ) {
                $ul['etablissement_siege']['unite_legale_id'] = $ul['id'];
            }
        }

        return $ul;

    }



	// Healthcheck
	function healthCheck()
	{
	    $data = dbHealthCheck();
	    return $data;
	}

    // dbHealthCheck : function added when db is populated
	function dbHealthCheck()
	{
		$db = new DB();

		$data=array();

		// Données établissement
		$sql = "SELECT *
		        FROM healthcheck();";

		// ==== Handle the request ====
		try {
            $data = $db->query($sql);
        } catch (PDOException $e) {
            return array(
                "status" => "unhealthy",
                "message" => "function healthcheck() not present in database"
                );
        }

        // Un seul recordset, on peut le retourner
        $data["status"] = "healthy";
        return $data;

		unset ($db);

	}




?>