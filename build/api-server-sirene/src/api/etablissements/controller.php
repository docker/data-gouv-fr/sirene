<?php

     require_once (__DIR__."/../model.php");

     /*
      * API Functions
      */


    // Get one item (Etablissement + UniteLegale + Siege)
	function _get($siret = null, $reqparam = null)
	{
		$data=array();

		// On a un ID
		// - un id (aucun autre caractère que numérique)
		// - on ignore "reqparam", qui pourrait servir de filtre complémentaire

		// Données établissement
		if (  $siret !== null   && strlen($siret) == 14   && ctype_digit($siret)  ) {

			$data['etablissement'] = getEtablissement($siret);

			if ( $data['etablissement'] === null  ) {

				return 404;

			} else {

				$db_start_time=microtime(true);

				// Données (simplifiées) sur l'unité légale
				$data['etablissement']['unite_legale'] = getUniteLegale($data['etablissement']['siren']);

				// Check si on a trouvé l'unité légale avant de l'enrichir
				if ( null !== $data['etablissement']['unite_legale'] ) {

					// Enrchissements pour coller à l'API data.gouv.fr
					// (unite_legale_id)
					$data['etablissement']['unite_legale_id'] = $data['etablissement']['unite_legale']['id'];

					// L'établissement siège de cette UL
					$data['etablissement']['unite_legale']['etablissement_siege'] = getEtablissement($data['etablissement']['siren'].$data['etablissement']['unite_legale']['nic_siege']);

					if ( null !== $data['etablissement']['unite_legale']['etablissement_siege'] ) {
						$data['etablissement']['unite_legale']['etablissement_siege']['unite_legale_id'] = $data['etablissement']['unite_legale']['id'];
					}

				} else {

					addSpoolItem("siren", $data['etablissement']['siren'], "ET-OK_UL-KO");
					$data['etablissement']['unite_legale'] = "not found - renouveler la demande dans une minute.";

				}

				$data['duration']['db'] = microtime(true) - $db_start_time;
				return $data;

			}

		} else {
			return 415;
		}

	}

	// Recherche multi-critères, pas de Siret fourni, juste des paramètres
	function _getAll($reqparam = null)
	{

		$db_start_time=microtime(true);

		// Analyse de $reqparam, qui peut être :
		// - un id (aucun autre caractère que numérique)
		// - un critère de recherche au format critere=valeur
		if ( is_array($reqparam) ) {

			// Le critère sert à récupérer un numéro de siret.
			$etablissements = getEtablissementsByParam($reqparam);

			if ( $etablissements === null ) {
				return 404;
			}
			else
			{

				$data=array();
				$data['etablissements'] = array();

				// Pour chacun, on recup toutes les infos
				foreach ($etablissements as $etablissement)
				{
					array_push($data['etablissements'], getEtablissement($etablissement['siret']));
				}

				$data['duration']['db'] = microtime(true) - $db_start_time;
				return $data;

			}

		}

		else // Pas de paramètre, comment peut-on en arriver là ?
		{

				return 415;
		}


	}

	// PUT (Etablissement) : demande de mise à jour
	function _put($siret = null)
	{

		if (  $siret !== null   && strlen($siret) == 14   && ctype_digit($siret)  ) {

			addSpoolItem("siret", $siret, "PUT called");
			return array("message" => "Mise à jour demandée pour etablissement " . $siret);

		} else {
			return 415;
		}

	}

	// putAll : demande à raffraichir les siret depuis une date donnée
	/**
	 * On joue sur la colonne date_dernier_traitement de la table status. 
	 * que l'on positionne à celle spécifiée.
	 */

	function _putAll($params = null)
	{

		if (  $params !== null   && is_array($params)  ) {

			if (array_key_exists("ddt",$params) && validateDate($params["ddt"]) ) {

				resetDDT("etablissement", $params["ddt"]);

			}

		} else {
			return 415;
		}

	}

?>