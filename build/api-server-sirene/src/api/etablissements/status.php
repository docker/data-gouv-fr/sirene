<?php

		$endpoint_status=array(
			"title" => "endpoint /etablissements",
			"definition" => "Teste la présence d'établissements dans la vue 'etablissement'",
			"vital" => true,
			"section" => 0,
			"level" => 1,
			"status" => "OK"
		);

		// Contrôle accès à la DB
		$db = new DB();

		try {
			$rs = $db->query("SELECT siret FROM etablissement LIMIT 1");

			if ( ( $rs == false ) || (count($rs) == 0) ) {

					$endpoint_status["status"] = "KO";

			}

		} catch (PDOException $e) {
			$endpoint_status["status"]="KO";
		}


?>