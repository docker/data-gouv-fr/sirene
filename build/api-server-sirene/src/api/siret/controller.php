<?php

     require_once (__DIR__."/../modelInsee.php");

     /*
	  * API Functions
	  */

    // Get one item (siret + UniteLegale + Siege)
	function _get($siret)
	{
		$data=array();

		if (  $siret !== null   && strlen($siret) == 14   && ctype_digit($siret)  ) {

			// Données établissement + adresse + périodes
			$data['etablissement'] = getInseeSiret($siret);
			if ( $data['etablissement'] === null  ) {

				return 404;

			} else {

				$db_start_time=microtime(true);

				// Données (simplifiées) sur l'unité légale
				$data['etablissement']['uniteLegale'] = getInseeUniteLegale($data['etablissement']['siren']);

				$data['duration']['db'] = microtime(true) - $db_start_time;
				return $data;

			}
		} else {
			return 415;
		}

	}

	// PUT (Etablissement) : demande de mise à jour
	function _put($siret = null)
	{

		if (  $siret !== null   && strlen($siret) == 14   && ctype_digit($siret)  ) {

			addSpoolItem("siret", $siret, "PUT called");
			return array("message" => "Mise à jour demandée pour siret " . $siret);

		} else {
			return 415;
		}

	}


?>