<?php

     /*
	 * API Functions
	 */
    // [GET] /status/
	function _get($id = null, $set_header = null)
	{

		$status = array();

		/*
		   Appel avec un paramètre de niveau : forcément numérique.
		    /status/1
			/status/2
		 */
		if (is_numeric($id))
		{
			return _getAll(null, max($id,1));
		}

		else
		/*
		   Appel pour un des endpoints connus
		 */
        {
			if ( is_dir("api/$id")
			&&   is_readable ("api/$id/status.php")) {

				$global_status = STATUS_OK_VALUE; // cf. api-base
				$status_message = "status disponible";

				require_once ("api/$id/status.php");

				$status = $endpoint_status;
				unset($endpoint_status);


				if ( $set_header !== false && $status["status"] !== STATUS_OK_VALUE ) {

					if ($status["vital"] === true) {

						header($_SERVER['SERVER_PROTOCOL'] . " 503 " . $status_message, true, 503);

					} else {

						header($_SERVER['SERVER_PROTOCOL'] . " 520 " . $status_message, true, 520);

					}
				}

			}

		}
		return $status;

	}

    // (PUT) /status/updater : update de la version du composant
	function _put($id = null)
	{

		$putdata = (array) json_decode(file_get_contents("php://input"));

		/*
		   Appel pour un des endpoints connus
		 */
		switch ($id) {

			case "updater":

				if ( array_key_exists("version", $putdata) ) {
					file_put_contents("updater.json", json_encode($putdata["version"]));
					$data["status"]="Updater Version received.";	
				}
				else
				{
					return 415;
				}
				break;

			default:
				return 403;
				break;

		}

		return $data;

	}


    // Get full status
	function _getAll($params = null, $level = 0 )
	{

		// Init du résultat
		$data = array(
			"api_version" => API_VERSION,
			"status" => "unknown",
			"sections" => (object) [
				"Endpoints",
				"Updater"
			],
			"sections_status" => array(),
			"probes" => array()
		);

		// Updater version : uniquement intéressant si 1 seul replica de api-server
		//                   Le fichier est créé sur requête PUT /status/updater
		if (is_readable ("updater.json") )
		{
			$data["updater_version"] = json_decode(file_get_contents("updater.json"), true);
		}


		foreach($data["sections"] as $s) {
			$data["sections_status"][$s] = "OK";
		}


		/*

            Section 1 : vérification des endpoints de l'API

		*/
		foreach (API_ENDPOINTS as $endpoint) {

			$probe = _get($endpoint, false);

			if ( ! empty($probe) ) {
				if ( $probe["status"] === "KO") {

					if ( $probe["vital"] === true )  {
						$data["sections_status"]["Endpoints"] = "KO";
					} else {
						if ($data["sections_status"]["Endpoints"] == "OK" ) $data["sections_status"]["Endpoints"] = "WARNING";
					}
				}
				array_push($data['probes'], $probe);
			}

		}

		/*

            Section 2 : vérification de l'updater pour chaque table

		*/
		// Contrôle accès à la DB
		$db = new DB();

		// Relation nom de table(vue) => vital
		$sirene_tablevitale=array(
			"rna" => false,
			"etablissement" => true,
			"unite_legale" => true,
			"succession" => false
		);


		// ==== Status des tables : la last_sync a-t-elle plus de 7 jours ? ====
		$dernier_run_updater=0;
		$date_ref = date("Y-m-d H:i:s", time() - STATUS_PARAM_UPDATER_MAX_AGE );

		try {
			$rs = $db->query("SELECT * FROM status");

			if ( $rs !== false ) {

				foreach ($rs as $item) {

					// On ne traite que les tables "vitales"
					if (! array_key_exists($item['table_name'], $sirene_tablevitale) ) continue;

					if ( is_array($item) && count($item) != 0 ) {

						// Pas de date dernier_traitement, on passe.
						if ( $item["last_sync"] == null ) continue;

						// Etat remonté par la table status
						$probe=array(
							"title" => "Updater: " . $item["table_name"],
							"definition" => "Verifie si la dernière synchro de la table '" . $item["table_name"] . "' a moins de 7 jours",
							"section" => 1,
							"vital" => $sirene_tablevitale[$item["table_name"]],
							"date_dernier_traitement" => $item["date_dernier_traitement"],
							"date_dernière_synchro" => $item["last_sync"]
						);
						// On va aussi vérifier à la fin que le moteur a tourné il y a moins de 48h
						$dernier_run_updater=max($item["last_sync"], $dernier_run_updater);

						// Contrôle...
						// Si last_sync existe alors elle doit être postérieure à date_ref, sinon warning.
						if ( $item["last_sync"] < $date_ref ) {
							$probe["status"] = 'KO';
							// si le test est vital, alors global KO
							if ( $probe["vital"] === true ) $data["sections_status"]["Updater"] = "KO";
						}
						else {
							$probe["status"] = 'OK';
						}

						array_push($data['probes'], $probe);

					} // Else rien, pas de données remontée

				}
			}

		} catch (PDOException $e) { $db_status="KO"; }

		// Retour des résultats
		$status = 'healthy';
		foreach ($data["sections_status"] as $section_status) {

			if ($section_status == 'KO') {
				$status = 'KO';
				break;
			}

			if ($section_status == 'WARNING') {
				$status = 'WARNING';
			}

		}

		$data["status"]=$status;

		switch($status) {

			case "KO":
					 	header($_SERVER['SERVER_PROTOCOL'] . " 503 " . $status, true, 503);
						break;
			case "WARNING":	
						header($_SERVER['SERVER_PROTOCOL'] . " 520 " . $status, true, 520);
						break;
		}

		return $data;
	}



?>
