<?php

     require_once (__DIR__."/../modelSpool.php");

     /*
      * API Functions
      */


    // Get one item item_id
	function _get($item_id = null, $reqparam = null)
	{
		$data=array();

		// On a un ID
		// - un id (aucun autre caractère que numérique)
		// - on ignore "reqparam", qui pourrait servir de filtre complémentaire
		if ($item_id !== null ) {

			$data["spoolitem"] = getSpoolItem($item_id);

			if ( $data === false  ) {

				return 404;

			} else {

				$db_start_time=microtime(true);

				$data['duration']['db'] = microtime(true) - $db_start_time;
				return $data;

			}

		}

	}

	// Liste les éléments en spool
	function _getAll($reqparam = null)
	{

		$db_start_time=microtime(true);

		// Le critère sert à récupérer un numéro de siret.
		$spool = getSpoolItems($reqparam);
		if ( $spool === false ) {
			return 404;
		}

		else
		{

			$data=array();
			$data['spool'] = $spool;
			$data['duration']['db'] = microtime(true) - $db_start_time;
			return $data;

		}

	}


    // Remove one item item_id
	function _delete($item_id = null, $reqparam = null)
	{

		// On a un ID ou "ALL"
		// - un id (aucun autre caractère que numérique)
		// - on ignore "reqparam", qui pourrait servir de filtre complémentaire
		if ($item_id !== null ) {

		  rmSpoolItem($item_id);

		}


	}


?>