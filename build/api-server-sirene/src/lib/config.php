<?php

    define("API_VERSION",  $_ENV["API_VERSION"] ?? "0.0");
    define("API_ENDPOINTS",  array( "etablissements",
                                    "unites_legales",
                                    "siret",
                                    "siren")
    );

    // Delai entre la dernière synchro de donnée et maintenant pour décréter que le statut de l'updater est KO
    // 1 semaine.
    define("STATUS_PARAM_UPDATER_MAX_AGE",  $_ENV["STATUS_PARAM_UPDATER_MAX_AGE"] ?? 604800);


    define("DISABLE_NON_DIFFUSIBLES",  isset($_ENV["DISABLE_NON_DIFFUSIBLES"]) ? true : false);

    define("STATUS_OK",      'OK' );
    define("STATUS_WARNING", 'WARNING' );
    define("STATUS_KO",      'KO' );


    // la DB
    $db_conn = 'pgsql:';

    $db_name = isset($_ENV["POSTGRES_DB_NAME"]) ? $_ENV["POSTGRES_DB_NAME"] : "sirene";
    $db_conn .= "dbname=" . $db_name;

    $db_host = isset($_ENV["POSTGRES_DB_HOST"]) ? $_ENV["POSTGRES_DB_HOST"] : "db";
    $db_conn .= ";host=" . $db_host;

    $db_port = isset($_ENV["POSTGRES_DB_PORT"]) ? $_ENV["POSTGRES_DB_PORT"] : "5432";
    $db_conn .= ";port=" . $db_port;

    define("DB_CONN",    $db_conn);
    define("DB_USER",    isset($_ENV["POSTGRES_DB_USER"])     ? $_ENV["POSTGRES_DB_USER"]     : "Missing POSTGRES_DB_USER envvar" );
    define("DB_PASS",    isset($_ENV["POSTGRES_DB_PASSWORD"]) ? $_ENV["POSTGRES_DB_PASSWORD"] : "Missing POSTGRES_DB_PASSWORD envvar" );

    define("MAX_ITEMS_PER_QUERY",    isset($_ENV["MAX_ITEMS_PER_QUERY"]) ? $_ENV["MAX_ITEMS_PER_QUERY"] : 100 );

?>
