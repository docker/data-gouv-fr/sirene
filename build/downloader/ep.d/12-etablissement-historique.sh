#!/bin/bash

# URL : https://files.data.gouv.fr/insee-sirene/StockEtablissementHistorique_utf8.zip

[ "${DO_ET_HISTO:-false}" == "false" ] && echo "$0: DO_ET_HISTO = false" && exit

# Téléchargement zip et extraction CSV

BASE_URL=https://files.data.gouv.fr/insee-sirene
FNAME=StockEtablissementHistorique_utf8.zip

# On travaille dans un volume persistant
cd /downloads
touch ${FNAME/zip/csv}.lock


# Téléchargement des fichiers si ils sont absents

# Le fichier est-il présent ?
if [ -f ${FNAME} ]; then

   MESSAGE="Fichier ${FNAME} déjà présent"
   # Vérification si fichier plus récent que celui déjà présent.
   # Attention, avec la commande "date" de busybox, c'est sioux !
   REMOTE_LastModified=$(date +%s --date="$(wget --method=HEAD -qS  ${BASE_URL}/${FNAME} 2>&1 | grep 'Last-Modified' | cut -d: -f2-)")
   LOCAL_LastModified=$(stat -c %Y ${FNAME})

   # DL uniquement si date Remote est plus récente
   if [ "${REMOTE_LastModified:-0}" -gt "${LOCAL_LastModified:-0}" ]; then

      MESSAGE=$MESSAGE", mais Upstream plus récent, "
      # echo "Local : $(date --date=@${LOCAL_LastModified}) / Distant : $(date --date=@${REMOTE_LastModified})."
      DL=true

   else

      MESSAGE=$MESSAGE" et à jour. Pas de téléchargement."
      # echo "Local : $(date --date=@${LOCAL_LastModified}) / Distant : $(date --date=@${REMOTE_LastModified})."
      DL=false

   fi

else

   # On a pas le fichier, on doit le DL
   DL=true
   MESSAGE="Fichier ${FNAME} absent."

fi
echo "$0: $MESSAGE"

# Si on doit télécharger, on le fait !
if [ ${DL:-false} = true ]; then
   echo "$0: téléchargement de ${FNAME}..."
   rm -f ${FNAME}
   if wget -c -o ${FNAME}-download.log -O ${FNAME} ${BASE_URL}/${FNAME}  ; then
      echo "$0: Téléchargement terminé... $(date)" >> ${FNAME}-download.log
      echo "$0: --> Téléchargement de ${FNAME} terminé."

   else
      echo "$0: Problème au téléchargement de ${FNAME}."
      cat ${FNAME}-download.log
      exit 1
   fi
   sync
fi

rm -f ${FNAME/zip/csv}.lock
sync
