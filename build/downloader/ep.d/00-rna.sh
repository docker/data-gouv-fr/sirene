#!/bin/bash
# URLS :
# - https://media.interieur.gouv.fr/rna/rna_import_YYYYMM01.zip
# - https://media.interieur.gouv.fr/rna/rna_waldec_YYYYMM01.zip

[ "${DO_RNA:-true}" == "false" ] && echo "$0: DO_RNA = false" && exit

BASE_URL=https://media.interieur.gouv.fr/rna

# Téléchargement des fichiers si ils sont absents
for F in import waldec; do

   # On supprime les anciens fichiers
   rm -f StockAssociations_${F}.zip

   # # Déterminer le nom et la date du dernier fichier disponible
   NM=0 ; CR=0
   while [ "${CR}" != 200  ]; do
      D=$(date +%Y%m01 --date="now ${NM} months")
      FNAME=rna_${F}_${D}.zip
      curl -Ls -w '%{stderr}%{http_code}' -I ${BASE_URL}/${FNAME} > /tmp/${FNAME}.log 2>/tmp/${FNAME}.err

      CR=$(cat /tmp/${FNAME}.err)
      if [ ${CR} = 404 ] ; then
         (( NM-- ))
      fi
      rm -f /tmp/${FNAME}.log /tmp/${FNAME}.err
      sleep 1
   done

   # On DL
   echo -e "$0: téléchargement de ${FNAME}... \c"

   # curl -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:33.0) Gecko/20100101 Firefox/33.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Referer: https://www.data.gouv.fr/fr/datasets/repertoire-national-des-associations/'  -H 'Connection: keep-alive'"
   wget -c -o StockAssociations_${F}-download.log -O StockAssociations_${F}.zip ${BASE_URL}/${FNAME}
   echo "Téléchargement terminé... $(date)" >> StockAssociations_${F}-download.log
   echo "OK."

done


#-H 'Cookie: all required cookies will appear here' \
