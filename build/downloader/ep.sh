#!/bin/bash

function pluriel()
{
  [ "$1" -gt 1 ] && echo -n "s"
}

function s2dhms()
{
   RESTE=${1:-0}
   echo -n "$(( D=RESTE / (24*3600) )) jour$(pluriel $D), " ; ((  RESTE-=(D*24*3600)  ))
   echo -n "$(( H=RESTE / 3600 )) heure$(pluriel $H), "     ; ((  RESTE-=(H*3600)     ))
   echo -n "$(( M=RESTE / 60 )) minute$(pluriel $M), "      ; ((  RESTE-=(M*60)       ))
   echo "$RESTE seconde$(pluriel $RESTE)."
}

echo "Identité: $(id -a)"

export DATE=$(date +%Y%m01)

# Purge des vieux zip
find /downloads -name '*.zip' -type f -mtime +30 -delete && echo "Vieux fichiers zip purgés"

# On supprime les vieux logs de téléchargement
find /downloads -name "*-download.log" -type f -mtime +30 -delete && echo "Vieux logs de téléchargement purgés"

# Exécution en // des scripts de DL
cd /downloads
touch download.lock

# Exécution en // des scripts de DL
echo "En attente du téléchargement des fichiers..."
for S in /ep.d/*.sh
do
    ${S}
done

# Flush
sync
echo "Done !"

# Permissions & suppression lock
find /downloads -type d -exec chmod 755 {} \;
find /downloads -type f -exec chmod 644 {} \;
rm -f download.lock
touch download-done

# Si mode Oneshot, on rend la main.
[ "${ONESHOT:-false}" = "true" ] && echo "Oneshot mode, exiting" && exit

# Sinon, on va attendre l'heure du prochain DL
if [ -z "${SLEEPTIME}" ]
then
   # On va patienter jusqu'au début du mois prochain
   NM=$(date +%Y/%m/02 --date="now 1 month")
   SLEEPTIME=$((   $(date +%s --date=${NM}) - $(date +%s)  + 43200 ))

   echo "Fichiers à jour. Relance à 12h00 le 2 du mois prochain : $(s2dhms ${SLEEPTIME})"
fi

# Temporisation (au bout de laquelle on est "restart")
exec sleep ${SLEEPTIME}
