<?php

    define("UPDATER_VERSION",  $_ENV["UPDATER_VERSION"] ?? "0.0");
    define ("MY_NAME", "[Updater - ". UPDATER_VERSION . "]");

    // la DB
    $db_conn = 'pgsql:';

    $db_name = isset($_ENV["POSTGRES_DB_NAME"]) ? $_ENV["POSTGRES_DB_NAME"] : "sirene";
    $db_conn .= "dbname=" . $db_name;

    $db_host = isset($_ENV["POSTGRES_DB_HOST"]) ? $_ENV["POSTGRES_DB_HOST"] : "db";
    $db_conn .= ";host=" . $db_host;

    $db_port = isset($_ENV["POSTGRES_DB_PORT"]) ? $_ENV["POSTGRES_DB_PORT"] : "5432";
    $db_conn .= ";port=" . $db_port;

    define("DB_CONN",    $db_conn);
    define("DB_USER",    isset($_ENV["POSTGRES_DB_USER"])     ? $_ENV["POSTGRES_DB_USER"]     : "Missing POSTGRES_DB_USER envvar" );
    define("DB_PASS",    isset($_ENV["POSTGRES_DB_PASSWORD"]) ? $_ENV["POSTGRES_DB_PASSWORD"] : "Missing POSTGRES_DB_PASSWORD envvar" );

    // l'API INSEE (SireneUpdater)
    define("API_INSEE_URL",   "https://api.insee.fr/api-sirene/" ); // ajout /api-sirene
    define("API_INSEE_VERSION",   "3.11" ); // V3.11 -> 3.11

    /**
     * curl -X 'GET' \
     *  'https://api.insee.fr/api-sirene/3.11/siren/439074154' \
     *  -H 'accept: application/json' \
     * -H 'X-INSEE-Api-Key-Integration: xxx-yyy....'
     */
    define("API_INSEE_APIKEY",   $_ENV["API_INSEE_APIKEY"] ??  null ); 

    // Limite : 30 requêtes par minute
    define("API_INSEE_MAXREQ_PER_PERIOD",     isset($_ENV["API_INSEE_MAXREQ_PER_PERIOD"])     ? $_ENV["API_INSEE_MAXREQ_PER_PERIOD"] : 30 ); // req/periode
    define("API_INSEE_PERIOD",                isset($_ENV["API_INSEE_PERIOD"])                ? $_ENV["API_INSEE_PERIOD"] : 60 ); // seconds
    define("API_INSEE_SLEEPTIME", 1); // seconds
    define("API_INSEE_MAXREQ_BURST", 6); // On s'autorise de temps en temps un petit "burst-mode" de quelques requêtes d'affilée.
        // Pour les interrogations de l'API
    define("API_INSEE_MAX_ITEMS_PER_REQUEST", isset($_ENV["API_INSEE_MAX_ITEMS_PER_REQUEST"]) ? $_ENV["API_INSEE_MAX_ITEMS_PER_REQUEST"] : 1000 ); // nb items/req

    // l'API ADRESSE (GeoSiret)
    define("API_ADRESSE_URL",   "https://api-adresse.data.gouv.fr" );

    // l'API-Server
    define("API_STATUS_URL", "http://api-status:8000/status");

    // Limite : 25 requêtes par seconde (https://api.gouv.fr/les-api/base-adresse-nationale)
    define("API_ADRESSE_MAXREQ_PER_PERIOD",     isset($_ENV["API_ADRESSE_MAXREQ_PER_PERIOD"])  ? $_ENV["API_ADRESSE_MAXREQ_PER_PERIOD"] : 25 ); // req/periode
    define("API_ADRESSE_PERIOD",                isset($_ENV["API_ADRESSE_PERIOD"])             ? $_ENV["API_ADRESSE_PERIOD"] : 1 ); // seconds
    define("API_ADRESSE_SLEEPTIME", 60); // seconds

    // Pour le mode "CSV"
    define("API_ADRESSE_CSV_CALLS_ENABLED",  isset($_ENV["API_ADRESSE_CSV_CALLS_ENABLED"])  ? $_ENV["API_ADRESSE_CSV_CALLS_ENABLED"]  : false  ); // items
    define("API_ADRESSE_MIN_CSV_LINES",  isset($_ENV["API_ADRESSE_MIN_CSV_LINES"])  ? $_ENV["API_ADRESSE_MIN_CSV_LINES"]  : 50  ); // items
    define("API_ADRESSE_MAX_CSV_LINES",  isset($_ENV["API_ADRESSE_MAX_CSV_LINES"])  ? $_ENV["API_ADRESSE_MAX_CSV_LINES"]  : 10000 ); // items

    // Pour le cycle du moteur d'update
    define("UPDATER_TIME_INTERVAL", isset($_ENV["UPDATER_TIME_INTERVAL"]) ? $_ENV["UPDATER_TIME_INTERVAL"] :  600 ); // seconds
    define("UPDATER_TIME_INTERVAL_SUCCESSION", isset($_ENV["UPDATER_TIME_INTERVAL_SUCCESSION"]) ? $_ENV["UPDATER_TIME_INTERVAL_SUCCESSION"] : 7 * 86400 ); // 7 days in seconds
    define("UPDATER_TIME_INTERVAL_ND", isset($_ENV["UPDATER_TIME_INTERVAL_ND"]) ? $_ENV["UPDATER_TIME_INTERVAL_ND"] : 7 * 86400 ); // 7 days in seconds

    // Boucle de l'updater toutes les minutes
    define("UPDATER_LOOP_DELAY",  isset($_ENV["UPDATER_LOOP_DELAY"])  ? $_ENV["UPDATER_LOOP_DELAY"]  : 60 ); // seconds
    define("UPDATER_SPOOL_PURGE_DELAY",  isset($_ENV["UPDATER_SPOOL_PURGE_DELAY"])  ? $_ENV["UPDATER_SPOOL_PURGE_DELAY"]  : 30 ); // days

    // Exécution des traitements lourds toutes les heures
    define("UPDATER_WAKEUP_DELAY",  isset($_ENV["UPDATER_WAKEUP_DELAY"])  ? $_ENV["UPDATER_WAKEUP_DELAY"]  : 3600 ); // seconds

    // L'updater doit-il appeler API Adresse pendant la synchro des Siret ?
    define("UPDATER_GEOCODE_DURING_SIRET_SYNC", isset($_ENV["UPDATER_GEOCODE_DURING_SIRET_SYNC"]) ? $_ENV["UPDATER_GEOCODE_DURING_SIRET_SYNC"] : false);

    // L'updater doit-il appeler [PUT] /status/updater ?
    define("UPDATER_SUBMIT_VERSION_TO_API", $_ENV["UPDATER_SUBMIT_VERSION_TO_API"] ?? false);

    // Date de départ pour la synchro (ne concerne que les non diffusibles) : 2015-07-29
    define("UPDATER_START_DATE", isset($_ENV["UPDATER_START_DATE"])  ? $_ENV["UPDATER_START_DATE"]  : "2015-01-01 00:00:00"); // https://www.legifrance.gouv.fr/loda/id/LEGIARTI000031028407/2016-01-01/
    define("UPDATER_ONESHOT_RUN", isset($_ENV["UPDATER_ONESHOT_RUN"]) ? $_ENV["UPDATER_ONESHOT_RUN"] : "false"); // Dev : Updater stops when first run ends


    // Synchro INSEE ou pas (juste la consommation du spool)
    define("UPDATER_INSEE_SYNC", isset($_ENV["UPDATER_INSEE_SYNC"]) ? $_ENV["UPDATER_INSEE_SYNC"] : "true");

    // Gestion de la mémoire dans le traitement des résultats des requêtes :
    // --> 80% de UPDATER_MEMORY_LIMIT qui est forcément définie.
    define("MAX_MEM_FOR_RS_LOAD", ceil($_ENV["UPDATER_MEMORY_LIMIT"] * 0.8) );

?>
