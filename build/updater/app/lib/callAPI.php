<?php

/*
 * returns a structure
    $resp = new stdClass();
    $resp->httpcode = $httpCode;
    $resp->httpmsg  = $error_status;
    $resp->data = $data;
 *
 */
//                                                    (req-data)
function CallAPI($method, $url, $headers = array (), $data = array() ) {

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER    , $headers );

    switch ($method) {
        case "GET":
            break;

        case "POST":
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;

        case "PUT":

            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        /*
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            break;
        */
    }
    $response = curl_exec($curl);

    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // Check the HTTP Status code
    switch ($httpCode) {
        case 200:
            $error_status = "200: Success";
            break;
        case 301:
            $error_status = "301: Redirect";
            break;
        case 403:
            $error_status = "403: Forbidden";
            break;
        case 404:
            $error_status = "404: API Not found";
            break;
        case 405:
            $error_status = "405: Method not allowed";
            break;
        case 429:
            $error_status = "429: Too many requests";
            break;
        case 500:
            $error_status = "500: servers replied with an error.";
            break;
        case 502:
            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
            break;
        case 503:
            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
            break;
        default:
            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
            break;
    }
    curl_close($curl);

    $resp=new stdClass();
    $resp->httpcode = $httpCode;
    $resp->httpmsg  = $error_status;

    // Check si le retour est du Json
    $data = json_decode($response);

    if ( is_object($data) )  {
        $resp->data = $data;
    } else {
        $resp->data = $response;
    }
    unset($response);
    return $resp ;
}






?>
