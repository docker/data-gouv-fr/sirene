<?php

require_once (__DIR__."/functions.php");
require_once (__DIR__."/callAPI.php");

class ApiAdresse
{

    public $apiInfos;

    public $apicalls;
    public $apitimestart;
    public $apicalls_by_s;

    public $ok_calls = 0;
    public $ko_calls = 0;

    public function __construct() {

        $this->apitimestart = microtime(true);

        // $this->apiInfos = $this->getInfos();

        // // Test si service indispo
        // if ( $this->apiInfos->httpcode == 404 ) {
        //     throw new \Exception("API indisponible.\n" . json_encode($this->apiInfos, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));
        // }

    }

    public function __destruct() {

        // Free resources

    }


    function getGeoInfos($adresse) {

        // Tempo si nécessaire (25 appels à l'API par seconde MAXI)
        while ( $this->apicalls > API_ADRESSE_MAXREQ_PER_PERIOD && $this->apicalls_by_s > API_ADRESSE_MAXREQ_PER_PERIOD/API_ADRESSE_PERIOD ) {
            sleep (API_ADRESSE_SLEEPTIME);
        }

        $headers = array();
        // $adresse = avenue+du+gresille+&postcode=49000
        $query = "?q=" . $adresse . "&limit=1";

        $response = CallAPI('GET', API_ADRESSE_URL .
                            "/search/" . $query,
                            $headers
        ) ;

        $this->apicalls++;
        $this->apicalls_by_s = $this->apicalls / (microtime(true) - $this->apitimestart);
        //echo $this->apicalls_by_s  . "\n";

        switch ( $response->httpcode ) {
            case 200:

                // Contrôle des données reçues, on sort avec un score à -999 si KO.
                if ( count($response->data->features) == 0 ) return array("geo_score" => -999);

                // On a des données
                $data = array(
                    "longitude"   => $response->data->features[0]->geometry->coordinates[0],
                    "latitude"    => $response->data->features[0]->geometry->coordinates[1],
                    "geo_score"   => $response->data->features[0]->properties->score,
                    "geo_type"    => $response->data->features[0]->properties->type,
                    "geo_adresse" => $response->data->features[0]->properties->label,
                    "geo_id"      => $response->data->features[0]->properties->id
                );
                return $data;

            case 500:
            case 504:
                    sleep (API_ADRESSE_SLEEPTIME);
                    return array("geo_score" => -999);

            default:

                return sprintf("%5.2f calls/s,  HTTP-%d (%s) : %s ", $this->apicalls_by_s, $response->httpcode, json_encode($response, JSON_UNESCAPED_UNICODE), $adresse) ;
                break;

        }

    }



// Class
}

?>