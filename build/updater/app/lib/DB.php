<?php

class DB
{

    public $conn;
    public $rowcount;

    public function __construct()
    {

        try {
            $this->conn = new PDO(DB_CONN , DB_USER, DB_PASS) ;
        }
        catch (PDOException $e) {
            error_log($e->getMessage());
            die(json_encode(
                array(
                        "message" => "Erreur interne: probleme de connexion a la base de donnees."
                    )
                )
            );
        };

    }

    public function __destruct()
    {
        unset($this->conn);
    }


    public function query($sql)
    {

        $resultset = $this->conn->prepare($sql);

        try {
            $rc = $resultset->execute();
        }
        catch (PDOException $e) {
            error_log($e->getMessage());
            error_log("Req: " . $sql);
            die(json_encode(
                array(
                        "message" => "Erreur interne: probleme lors de l'execution de la requete en base de donnees."
                    )
                )
            );
        };

        // Si la requête se passe bien : c'était un SELECT ou autre-chose ?
        if ( $rc === true ) {

            $this->rowcount = $resultset->rowCount();

            // si SELECT (1er mot de $sql), on retourne un tableau associatif avec les résultats
            switch ( strtoupper(strtok($sql, " ")) ) {

                case "SELECT":

                    // en nfonction du ombre de résultats, on formate le retour
                    switch ($this->rowcount) {

                        case 0: // --> no results found
                            return false;
                            break;

                        case 1: // --> 1 item
                            return $resultset->fetch(PDO::FETCH_ASSOC);
                            break;

                        default: // Plusieurs enregistrements, on les retourne dans un tableau
                            $data = array();
                            for ($i=1; $i<=$this->rowcount; $i++ ) {
                                array_push ($data, $resultset->fetch(PDO::FETCH_ASSOC));
                            };
                            return $data;
                            break;
                    }
                    break;

                // sinon c'est pas un select, on retourne le nombre d'éléments impactés.
                default:
                    return $this->rowcount;
                    break;
            }

        } else {

            return false;

        }
    }
}
