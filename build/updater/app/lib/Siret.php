<?php

class Siret
{

    public $_db;
    public $tablename;
    public $viewname;

    public $last_sync;
    public $dernier_traitement;

    public function __construct($db = null) {

        $this->_db = $db;

        $this->viewname = "etablissement";
        $this->tablename = "etablissement";

        $status = $this->_db->getStatus($this->tablename);
        $this->dernier_traitement = $status['date_dernier_traitement'];

    }

    public function __destruct() {

        // Free resources
        unset($this->_db);

    }


    // Input : stdClass Object
    // (
    //     [numeroVoieEtablissement] => 21
    //     [typeVoieEtablissement] => RUE
    //     [libelleVoieEtablissement] => DE SEINE
    //     [codePostalEtablissement] => 75006
    //     [libelleCommuneEtablissement] => PARIS 6
    //     [codeCommuneEtablissement] => 75106
    // )
    //    ==>         // (string) 21+RUE+DE+SEINE&postcode=75006

    function getAdresse( $object ) {

        // Adresse à l'étranger, pas de recherche d'adresse
        if ( property_exists($object, "codePaysEtranger")
             && ( $object->numeroVoieEtablissement) == null ) {
                return false;
        }

        // Pas de code Pays Etranger, on recherche l'adresse
        $parts = array();

        if ( property_exists($object, "numeroVoieEtablissement")
             &&  strlen(preg_replace('/ /','',$object->numeroVoieEtablissement)) != 0) {
                array_push($parts, str_replace(['"',"'", " "], "", $object->numeroVoieEtablissement));
        }

        if ( property_exists($object, "typeVoieEtablissement")
             &&  strlen(preg_replace('/ /','',$object->typeVoieEtablissement)) != 0)    {
                array_push($parts, str_replace(['"',"'", " "], "", $object->typeVoieEtablissement));
        }

        if ( property_exists($object, "libelleVoieEtablissement")
             && strlen(preg_replace('/ /','',$object->libelleVoieEtablissement)) != 0) {
                array_push($parts, str_replace(['"',"'"], "", str_replace(" ","+", $object->libelleVoieEtablissement)));
        }

        if ( property_exists($object, "complementAdresseEtablissement")
             && strlen(preg_replace('/ /','',$object->complementAdresseEtablissement)) != 0) {
                array_push($parts, str_replace(['"',"'"], "", str_replace(" ","+", $object->complementAdresseEtablissement)));
        }

        // Pas d'information sur l'adresse, pas de recherche d'adresse
        if (count($parts) == 0) return false;

        if ( property_exists($object, "codePostalEtablissement")
             && strlen(preg_replace('/ /','',$object->codePostalEtablissement)) != 0)  {

                // Certaines adresses ne comportent que ZI, et il faut 3 caractères mini en req sur la BAN
                // Une adresse doit commencer par un numérique ou un caractère alphabétique,
                // --> on retire ce qui ne l'est pas
                $adresse = preg_replace("/^[^a-z0-9]+/i", "",
                             str_replace(
                               array("ZI",
                                     "Z I",
                                     "ZA"),
                               array("Zone+Industrielle",
                                     "Zone+Industrielle",
                                     "Zone+Activite"),
                               implode("+", $parts)
                              )
                            );

                if (strlen($adresse) < 3 ) return false;

                // on a une adresse "valide"
                $adresse .= "&postcode=" . $object->codePostalEtablissement;
                return $adresse;
        }
        else
        // Pas de codePostalEtablissement, pas de recherche d'adresse
        {
            return false;
        }


    }


    function DBupdate( $item_in ) {

        if (property_exists($item_in, "statutDiffusion") && $item_in->statutDiffusion == "N") {

            $insert_cols="(siret, statut_diffusion)";
            $insert_values="('" . $item_in->siret. "','N')";
            $update = "SET statut_diffusion = 'N'";
            $sql = "INSERT INTO " . $this->tablename . $insert_cols  . " VALUES " . $insert_values ." ON CONFLICT (siret) DO UPDATE " . $update;

        }
        else
        {
            // siret/15100002301100
            if ( property_exists($item_in, "periodesEtablissement") ) {

                $item_in->periodes = $item_in->periodesEtablissement[0];
                unset($item_in->periodesEtablissement);

            }

            $item_out= keys_upper_to_underscore($item_in);


            // siret/15100002301100 ==> Pas de propriété siren...(cf Insee) -> on on rattache à substr(9)
            if ( ! array_key_exists('siren', $item_out) ) {
                $item_out['siren'] = substr($item_out['siret'],0,9);
                $item_out['nic'] = substr($item_out['siret'],9,5);
            }

            // Les blocs adresse et adresse_2 sont à convertir :
            // ==> toutes les clés qu'ils contiennent descendent d'un niveau et renommées
            if ( array_key_exists('adresse', $item_out) ) {

                foreach ($item_out['adresse'] as $key => $value ) {
                    $item_out[$key] = str_replace('"', "", $value ?? '');
                }
                unset($item_out['adresse']);

            }

            if ( array_key_exists('adresse_2', $item_out) ) {

                foreach ($item_out['adresse_2'] as $key => $value ) {
                    $item_out[$key] = str_replace('"', "", $value ?? '');
                }
                unset($item_out['adresse_2']);
            }

            // le bloc periodes[][''] (contient des indicateurs sur les changements dans l'ET)
            // On ne conserve pas les données de type "changement_" (absentes de la table)
            // Mais il indique notamment un changement d'état administratif de l'ET.
            // ==> on le descend à la base

            if ( array_key_exists('periodes', $item_out) ) {

                foreach ($item_out['periodes'] as $key => $value ) {
                    if ( strpos($key, 'changement_' ) === false ) {
                        $item_out[$key] = $value;
                    }
                    unset($item_out['periodes'][$key]);
                }
                unset($item_out['periodes']);
            }

            // Si on a pas de date de dernier traitement sur cet établissement, on prend celle de l'UL,
            // ou à défaut, la date_debut de cet etablissement

            if ( ! isset($item_out['date_dernier_traitement'])) {
                $item_out['date_dernier_traitement'] = $item_out['unite_legale']['date_dernier_traitement'] ?? $item_out['date_debut'] ?? null;
            }

            // Gestion de created_at et updated_at
            $item_out['created_at'] = $item_out['date_dernier_traitement'];
            $item_out['updated_at'] = $item_out['date_dernier_traitement'];

            // Constituer une requête d'upsert avec toutes les propriétés présentes, sauf celles de type array()
            $insert_cols = "(";
            $insert_values = "(";
            $update = "SET ";
            foreach ($item_out as $key => $value)
            {
                if ( ! is_array($value) )
                {
                    // Cols
                    $insert_cols .= "$key, ";

                    // Values : On quote les strings
                    if (is_string($value)) {
                        $insert_values .=  $this->_db->conn->quote($value) . ", " ;

                    } elseif ( is_bool($value)) {
                        $bool = $value ? 'true' : 'false' ;
                        $insert_values .= "$bool , ";

                    } elseif ( null === $value ) {
                        $insert_values .= "null , ";

                    } else {
                        $insert_values .= "$value, ";
                    }

                    $update .= "$key=";
                    $update .= "EXCLUDED.$key, ";

                }

            }

            // Retrait de la dernière virgule --> espace
            $insert_cols = preg_replace('/, $/', ')', $insert_cols);
            $insert_values = preg_replace('/, $/', ')', $insert_values);
            $update = preg_replace('/, $/', ' ', $update);

            $sql = "INSERT INTO " . $this->tablename . $insert_cols . " VALUES " . $insert_values ." ON CONFLICT (siret) DO UPDATE " . $update;
        }

        // On exécute cette requête.
        $res = $this->_db->query($sql);
        if ( $res !== false ) {
            $this->last_sync = date("Y-m-d H:i:s");

            if (isset($item_out['date_dernier_traitement'])) {
                $this->dernier_traitement = max($this->dernier_traitement, $item_out['date_dernier_traitement']) ;
            }
            // printf("DEBUG - %s -> %s\n", $item_out['siret'], $item_out['date_dernier_traitement']);
        };

        //printf(", INSEE -> statut_diffusion = %s", $item_out["statut_diffusion"]);
        return $res;
    }




// Class
}


?>
