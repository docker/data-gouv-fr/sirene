<?php

class Siren
{

    public $_db;
    public $tablename;
    public $viewname;

    public $last_sync;
    public $dernier_traitement;

    public function __construct($db = null) {

        $this->_db = $db;

        $this->viewname = "unite_legale";
        $this->tablename = "unite_legale";

        $status = $this->_db->getStatus($this->tablename);
        $this->dernier_traitement = $status['date_dernier_traitement'];
    }

    public function __destruct() {

        // Free resources
        unset($this->_db);

    }



    function DBupdate( $item_in ) {

        // ND ?
        if (property_exists($item_in, "statutDiffusion") && $item_in->statutDiffusion == "N") {

            $insert_cols="(siren, statut_diffusion)";
            $insert_values="('" . $item_in->siren. "','N')";
            $update = "SET statut_diffusion = 'N'";

            $sql = "INSERT INTO " . $this->tablename . $insert_cols  . " VALUES " . $insert_values ." ON CONFLICT (siren) DO UPDATE " . $update;


        }

        // le bloc periodes[] (contient des indicateurs sur les changements aux différentes périodes)
        // On ne conserve pas les données de type "changement_" (absentes de la table)
        // Mais il indique les propriétés qui ont changé dans la période, ex: changement d'état administratif.
        // ==> on le descend à la base
        else
        {


            // siren/151000023
            if ( property_exists($item_in, "periodesUniteLegale") ) {

                $item_in->periodes = $item_in->periodesUniteLegale[0];
                unset($item_in->periodesUniteLegale);

            }

            $item_out= keys_upper_to_underscore($item_in);

            if ( array_key_exists('periodes', $item_out) ) {

                foreach ($item_out['periodes'] as $key => $value ) {
                    if ( strpos($key, 'changement_' ) === false ) {
                        $item_out[$key] = $value;
                    }
                }
                unset($item_out['periodes']);

            }

            // Constituer une requête d'upsert avec toutes les propriétés présentes, sauf celles de type array()
            $insert_cols = "(";
            $insert_values = "(";
            $update = "SET ";
            foreach ($item_out as $key => $value)
            {
                if ( ! is_array($value) )
                {
                    // Cols
                    $insert_cols .= "$key, ";

                    // Values : On quote les strings
                    if (is_string($value)) {
                        $insert_values .=  $this->_db->conn->quote($value) . ", " ;
                        // $update .= "'$value', ";

                    } elseif ( is_bool($value)) {
                        $bool = $value ? 'true' : 'false' ;
                        $insert_values .= "$bool , ";
                        // $update .= "$bool , ";

                    } else {
                        $insert_values .= "$value, ";
                        // $update .= "$value, ";
                    }

                    $update .= "$key=";
                    $update .= "EXCLUDED.$key, ";

                }

            }

            // Retrait de la dernière virgule --> espace
            $insert_cols = preg_replace('/, $/', ')', $insert_cols);
            $insert_values = preg_replace('/, $/', ')', $insert_values);
            $update = preg_replace('/, $/', ' ', $update);

            $sql = "INSERT INTO " . $this->tablename . $insert_cols . " VALUES " . $insert_values ." ON CONFLICT (siren) DO UPDATE " . $update;

        }

        // On exécute cette requête.
        $res = $this->_db->query($sql);
        if ( $res !== false ) {
            $this->last_sync = date("Y-m-d H:i:s");

            if (isset($item_out['date_dernier_traitement'])) {
                $this->dernier_traitement = max($this->dernier_traitement, $item_out['date_dernier_traitement']) ;
            }
        };

        return $res;
    }




// Class
}



?>
