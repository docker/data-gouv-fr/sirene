<?php

class Succession
{

    public $_db;
    public $tablename;
    public $viewname;

    public $last_sync;
    public $dernier_traitement;

    public function __construct($db = null) {

        $this->_db = $db;

        $this->viewname = "succession";
        $this->tablename = "succession";

    }

    public function __destruct() {

        // Free resources
        unset($this->_db);

    }



    function DBupdate( $item_in ) {

        $item_out= keys_upper_to_underscore($item_in);

        // Quelques clés à renommer
        $item_out['siret'] = $item_out['siret_predecesseur'];
        unset ($item_out['siret_predecesseur']);

        $item_out['date'] = $item_out['date_lien_succession'] ;
        unset ($item_out['date_lien_succession']);

        $item_out['date_dernier_traitement'] = $item_out['date_dernier_traitement_lien_succession'] ;
        unset ($item_out['date_dernier_traitement_lien_succession']);

        // Constituer une requête d'upsert avec toutes les propriétés présentes, sauf celles de type array()
        $insert_cols = "(";
        $insert_values = "(";
        $update = "SET ";
        foreach ($item_out as $key => $value)
        {
            if ( ! is_array($value) )
            {
                // Cols
                $insert_cols .= "$key, ";

                // Values : On quote les strings
                if (is_string($value)) {
                    $insert_values .=  $this->_db->conn->quote($value) . ", " ;
                    // $update .= "'$value', ";

                } elseif ( is_bool($value)) {
                    $bool = $value ? 'true' : 'false' ;
                    $insert_values .= "$bool , ";
                    // $update .= "$bool , ";

                } else {
                    $insert_values .= "$value, ";
                    // $update .= "$value, ";
                }

                $update .= "$key=";
                $update .= "EXCLUDED.$key, ";


            } // On ne s'intéresse pas aux sous-objets (unite_legale)

        }

        // Retrait de la dernière virgule --> espace
        $insert_cols = preg_replace('/, $/', ')', $insert_cols);
        $insert_values = preg_replace('/, $/', ')', $insert_values);
        $update = preg_replace('/, $/', ' ', $update);

        // Upsert
        //$sql = "INSERT INTO " . $this->tablename . $insert_cols . " VALUES " . $insert_values ." ON CONFLICT (siret,siret_successeur) DO UPDATE " . $update;

        // Simple insert
        $sql = "INSERT INTO " . $this->tablename . $insert_cols . " VALUES " . $insert_values ;

        // On exécute cette requête.
        $res = $this->_db->query($sql);
        if ( $res !== false ) {
            $this->last_sync = date("Y-m-d H:i:s");

            if (isset($item_out['date_dernier_traitement'])) {
                $this->dernier_traitement = max($this->dernier_traitement, $item_out['date_dernier_traitement']) ;
            }
        };
        return $res;
    }




// Class
}


?>
