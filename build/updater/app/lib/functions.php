<?php

    // Convertit une chaine format CamelCase en uncerscore_case
    // --> retire les mots "terminaisons" avant le traitement
    // --> une valeur numérique en fin de conversion est préfixée par un "_"
    function uppercase_to_underscore($input)
    {
        $terminaisons = [
            'Etablissement',
            'UniteLegale'
        ];

        if (is_string($input)) {

            $output = $input;

            // Suppression des terminaisons éventuelles
            $output = str_replace($terminaisons, '', $output);

            // Conversion PascalCase to pascal_case
            $output = ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $output)), '_');

            // Ajout d'un _ avant un numérique de fin
            $output = preg_replace('/([0-9]+)$/', '_$0', $output);

            return $output;

        } else return;

    }

    // Convertit de CamelCase à underscore_case toutes les keys du jeu de données passé en entrée
    // (récursif)
    function keys_upper_to_underscore($input)
    {
        $data = array();

        foreach ($input as $key => $value )
        {
            $newkey = uppercase_to_underscore($key);
            if ( is_object ($value) || is_array ($value) ) {
                $data[$newkey] = keys_upper_to_underscore($value);
            }
            else
            {
                $data[$newkey]=$value;
            }
        }

        return $data;
    }



?>