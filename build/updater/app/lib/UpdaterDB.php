<?php


class UpdaterDB extends DB
{

    public $rows;

    private $schema = array(

        'spool' => "CREATE TABLE spool (
            item_type varchar(32) ,
            item_id varchar(14),
            status varchar(24) default NULL,
            created_at TIMESTAMP default (now() at time zone 'utc'),
            updated_at TIMESTAMP default NULL,
            reason varchar(16) default NULL,
            PRIMARY KEY (item_id)
            );",

        'status' => "CREATE TABLE status (
            table_name varchar(32),
            table_rows int,
            date_dernier_traitement TIMESTAMP default null,
            last_update TIMESTAMP default null,
            last_sync   TIMESTAMP default null,
            PRIMARY KEY (table_name)
            );"
    );

    public function getColor()
    {

        $res = $this->query("SELECT color FROM dbcolor();");
        if ( $res !== false ) {

            return $res['color'];

        } else {

            return "gray";

        }
    }

    /*
        détermine si une vue pointe vers une table 'blue' ou 'green', retourne false si vue inexistante ou couleur inattendue.
    */
    public function getTableColor($table = null)
    {
        // Need a tablename...
        if ($table === null) {
            return false;
        }

        $res = $this->query("SELECT pg_get_viewdef('$table', true) viewdef;");

        if ( $res !== false ) {

            return str_replace($table . "_", "", preg_replace('/^.*FROM (\w+)[ ;]*$/i', '$1', str_replace(array("\r","\n"),"",$res["viewdef"])));

        } else {

            return "gray";

        }
    }

    /*
        vérifie la présence des tables prévue dans le tableau "schena"
    */
    public function checkTables()
    {

        foreach ( $this->schema as $key => $sql) {

            $data = $this->query("SELECT count(*) from pg_tables WHERE tablename='$key';");

            if ($data === false ) return false;

            // Si pas de table, on la crée
            if ( $data['count'] == 0 ) {

                $this->query($sql);

            }

        }

    }

    /*
        assure la présence d'une colonne dans une tables
    */
    public function checkTableColumn($table = null, $column = null, $type = null)
    {

        if ( ( $table === null ) || ($table === null) || ($type === null) ) return false;

        $sql = "ALTER TABLE $table ADD COLUMN IF NOT EXISTS $column $type";
        $this->query($sql);

    }


    /*
        retourne un tableau d'éléments mis en spool
    */
    public function getSpooledItems()
    {

        $data = $this->query("SELECT * from spool WHERE status is NULL ORDER by created_at ASC;");

        if ($data === false ) return false;

        // Check nn records...
        if ( $this->rowcount  == 1 ) {

            return array("data" => $data);

        } else {

            return $data;
        }

    }

    /*
        MAJ d'un enregistrment du spool
    */
    public function updateSpooledItem($item = null, $status = "done")
    {

        // Need an item...
        if ($item === null) {
            return false;
        }

        if ( $status == "nd" ) {

            $res = $this->query("UPDATE spool set item_type='nd" . $item['item_type'] . "', status='". $status. "', updated_at = now() at time zone 'utc' WHERE item_type = '" .$item['item_type'] . "' AND item_id ='" . $item['item_id']. "';");

        } else {

            $res = $this->query("UPDATE spool set status='". $status. "', updated_at = now() at time zone 'utc' WHERE item_type = '" .$item['item_type'] . "' AND item_id ='" . $item['item_id']. "';");

        }

    }

    /*
        Supprime un élément du spool
    */
    public function rmSpooledItem($item = null)
    {

        // Need an item...
        if ($item === null) {
            return false;
        }
        $res = $this->query("DELETE FROM spool where item_type = '" .$item['item_type'] . "' AND item_id ='" . $item['item_id']. "';");
        return $res;
    }

    /*
        Supprime des vieux éléments du spool
    */
    public function pruneSpool($days = UPDATER_SPOOL_PURGE_DELAY)
    {

        $res = $this->query("DELETE FROM spool where updated_at < NOW() - INTERVAL '$days days';");

    }

    // S'assure qu'on a bien une date_dernier_traitement pour chaque nom de table présent dans la table "status"
    // Renseigne date_dernier_traitement si celle-ci est nulle.
    public function initStatus()
    {
        // Updater tables
        // $UPDATER_TABLES = array( "unite_legale", "etablissement", "succession", "unite_legale_nd", "etablissement_nd" );
        $UPDATER_TABLES = array( "unite_legale", "etablissement", "succession");

        // Comptages pour chacune des tables concernées
        foreach ($UPDATER_TABLES as $table) {

            $status = $this->getStatus($table);

            if ( ( $status === false ) || ( $status['date_dernier_traitement'] === NULL ) ) {

                // recherche de la date de Dernier Traitement
                $sql = "SELECT max(date_dernier_traitement) dernier_traitement FROM " . $table;
                $res = $this->query($sql);
                $dernier_traitement =  $res['dernier_traitement'] ?? UPDATER_START_DATE;

                // On stocke dans le status
                $sql = "INSERT INTO status (table_name, date_dernier_traitement )
                VALUES ('$table', '$dernier_traitement')
                ON CONFLICT (table_name) DO UPDATE
                    SET date_dernier_traitement = '$dernier_traitement' ";

                // On exécute cette requête.
                $dummy = $this->query($sql);

            }

        }

        return true;
    }

    // get status
    function getStatus($table_name) {

        $sql = "select * from status
                WHERE  table_name  = '" . $table_name . "';";

        // On exécute cette requête.
        $res = $this->query($sql);

        return $res;

    }

    // Update status
    function updateStatus($table_name, $column = null, $value = null) {

        if ( $column !== null && $value !== null) {

            $sql = "UPDATE status
                    SET    $column     = '" . $value . "'
                    WHERE  table_name  = '" . $table_name . "';";

            // On exécute cette requête.
            $res = $this->query($sql);

            return $res;
        }

    }

    public function updateCount($table)
    {

        // Count
        $sql = "SELECT count(*) table_rows FROM $table";
        $res = $this->query($sql);

        $this->rows = $res['table_rows'] ?? -1;

        $sql = "INSERT INTO status (table_name, table_rows)
                VALUES ('$table',  $this->rows  )
                ON CONFLICT (table_name) DO UPDATE
                    set  table_rows  = $this->rows  ";

        // On exécute cette requête.
        $dummy = $this->query($sql);

        return true;
    }
}
