<?php

require_once (__DIR__."/functions.php");
require_once (__DIR__."/callAPI.php");

/*
 * Gestion des GEOScores :
 *     -999 : on a rencontré une erreur lors du geocodage
 *     0 : non géo-codé, donc à géocoder plus tard
 *     autre : on a un geocodage issu de la BAN.
 */
define("FILTRE_GEO_SIRET", " WHERE date_dernier_traitement > '" . UPDATER_START_DATE . "' AND etat_administratif != 'F' AND (longitude is null OR latitude is null) AND (geo_score is null OR geo_score != -999) AND code_postal is not null ");

class MassGeoSiret
{

    public $apiInfos;

    public $apicalls;
    public $apitimestart;
    public $apicalls_by_s;

    public $todo = 0;
    public $done = 0;
    public $ko = 0;

    public function __construct($db = null) {


        $this->apiInfos = $this->getInfos();

        // Test si service indispo
        if ( $this->apiInfos->httpcode == 404 ) {
            throw new \Exception("API indisponible.\n" . json_encode($this->apiInfos, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));
        }


        $this->siret = new Siret($db);

        $this->todo = $this->countUnlocalizedItems('siret');
        $this->apitimestart = microtime(true);

    }

    public function __destruct() {

        // Free resources
        unset($this->siret);

    }


    function getInfos() {

        $headers = array();
        $query = "?q=avenue+du+gresille+&postcode=49000&limit=1";

        $response = CallAPI('GET', API_ADRESSE_URL .
                            "/search/" . $query,
                            $headers
        );
        $this->apicalls++;

        switch ( $response->httpcode ) {
            case 200:
            case 404:
                return $response;
                break;

            default:
                throw new \Exception("Erreur lors de la requete vers l'API.\n" . json_encode($response, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));

        }

    }

    public function countUnlocalizedItems($classe = 'siret') {

        $sql = "SELECT count(*) num
                  FROM " . $this->$classe->tablename . FILTRE_GEO_SIRET ;

        $res = $this->$classe->_db->query($sql);
        if ( $res !== false ) {
            if ( strlen($res['num']) != 0 ) {

                return $res['num'];

            } else {

                throw new \Exception("La base semble vide.");
            }

        };

    }


    public function createCSV($limit = 500, $classe = 'siret') {

        // Req
        $sql = "SELECT siret, complement_adresse, numero_voie, type_voie, libelle_voie, code_postal postcode, code_commune citycode, libelle_commune city
                  FROM " . $this->$classe->tablename  . FILTRE_GEO_SIRET . " LIMIT " . $limit;

        $res = $this->$classe->_db->query($sql);
        if ($res == null || $res == false) return false;

        if (is_array($res) && count($res) > 1) {

            // CSV export
            $fname = tempnam("/tmp", "GeoSiret_");
            $fp = fopen($fname, 'w');

            // Titres
            fputcsv($fp, array_keys($res[0]));

            // Données
            foreach ($res as $fields) {
                // On colle les lettres isolées les unes aux autres s'il en existe (1 seule passe)
                fputcsv($fp, preg_replace("/ (.) (.) /", " $1$2 ", $fields) );
            }

            fclose($fp);
            return $fname;

        } else {
            return false;
        }

    }



    // Récupération d'un tableau avec les coordonnées des items
    // Entrée : fichier CSV
    // Retour : tableau de siret géolocalisés
    function getGeoData($csv = null) {

        $data = false;

        if ($csv === null) return false;
        if (! is_file($csv) ) return false;

        // Pas de header spécifique pour l'API Adresse
        $headers=array();

        // Données pour l'API Adresse /search/csv :
        // - le fichier CSV
        // - les colonnes résultat souhaitées (siret, longitude, latitude...)
        $args['data'] = new CurlFile($csv, 'text/csv');

        // Appel de l'API
        $response = CallAPI( 'POST',
                             API_ADRESSE_URL . "/search/csv/" ,
                            $headers,
                            $args
                    );
        $this->apicalls++;


        // Test du CR : on risque la 404 si aucun élément trouvé.
        switch ( $response->httpcode ) {

            case "200":  // On a des données... mais c'est du CSV

                // L'API retourne du CSV, on en fait un tableau
                $content = array_map("str_getcsv", explode("\n", $response->data));
                $headers = $content[0];
                $data = array();

                foreach ($content as $row_index => $row_data) {
                    if($row_index === 0) continue;

                    $item = array();

                    foreach ($row_data as $col_idx => $col_val) {
                        $label = $headers[$col_idx];
                        $item[$label] = $col_val;
                    }

                    // En cas de dernière ligne vide...
                    if ( $item['siret'] !== null ) {
                        // On alimente la stucture résultat
                        array_push($data, $item);
                    }
                    unset ($item);

                };
                break;


            case "404": // Pas de données, on ne fait rien, mais ce n'est pas une erreur.
                break;


            case "429": // On a été trop vite...
                printf ("[HTTP-429] Too Many Requests... Tempo %d secondes\n", API_ADRESSE_SLEEPTIME);
                sleep (API_ADRESSE_SLEEPTIME);
                break;


            case "502": // Plus grave ?
                printf ("[HTTP-502] function getGeoData(), exiting.\n");
                exit (1);
                break;


            default:
                printf ("[HTTP" . $response->httpcode . "] ... Tempo %d secondes\n", API_ADRESSE_SLEEPTIME);
                sleep (API_ADRESSE_SLEEPTIME);

        }


        unset ($response);

        return $data;

    }


    public function updateGeoData($data = array(), $classe = 'siret') {

        $res = false;
        foreach ($data as $item) {

            // On vérifie les données retournées :
            // - citycode == result_citycode
            // - postcode == result_postcode
            if ($item['siret'] === null ) return false;

            if ( array_key_exists('result_postcode', $item )
              && array_key_exists('result_citycode', $item )
              && $item['postcode'] == $item['result_postcode']
              && $item['citycode'] == $item['result_citycode']
              ) {

                // Req
                $sql = "UPDATE " . $this->$classe->tablename
                    . "   SET latitude   = " . $item['latitude']
                    . "   ,   longitude  = " . $item['longitude']
                    . "   ,   geo_score  = " . $item['result_score']
                    . "   ,   geo_type   = '" . $item['result_type'] . "'"
                    . "   ,   geo_adresse= '" . str_replace("'", "''", $item['result_name']) . "'"
                    . "   ,   geo_id     = '" . $item['result_id']   . "'"
                    . "   ,   updated_at = now()"
                    . " WHERE siret      = '" . $item['siret'] . "';"
                    ;

            } else {

                // Req
                $sql = "UPDATE " . $this->$classe->tablename
                    . "   SET geo_score = -999 "
                    . "   ,   updated_at = now()"
                    . " WHERE siret     = '" . $item['siret'] . "';"
                    ;
                $this->ko++;

            }


            // Update de la DB
            $res = $this->$classe->_db->query($sql);
            if ( $res === false) {
                print_r($item);echo "\n";
                print_r($sql); echo "\n";

                throw new \Exception("Erreur lors de la requete updateGeoData en DB.\n" . json_encode($item, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));

            }
            $this->done++;

        }

        return $res;

    }


    /*
        Création du CSV
        Appel à l'API
        Maj de la DB
     */
    public function geoCodage() {

        $csv = null;

        // nb d'items par requête :  auto ajustable
        //$geo_items = ceil( (API_ADRESSE_MIN_CSV_LINES + API_ADRESSE_MAX_CSV_LINES) / 2);
        $geo_items = API_ADRESSE_MAX_CSV_LINES;

        while ( $csv !== false ) {

            // Production du CSV
            $csv = $this->createCSV($geo_items); // Params : Max items, returns filename ou false

            // $csv est false si on a aucun enregistrement à geolocaliser
            if ($csv !== false) {

                // Récupération des données de geolocalisation à partir du CSV
                $data = $this->getGeoData($csv);

                // Data est false si problème type 502 avec l'API : un réduit drastiquement le nombre d'items.
                if ( $data === false ) {

                    echo "GeoSiret [Error] Probleme sur les données retournées, on ralentit...\n";
                    $geo_items = max(API_ADRESSE_MIN_CSV_LINES, floor($geo_items / 10 )) ;

                } else {

                    // Stockage dans la DB
                    $res = $this->updateGeoData($data);
                    // if ($res === false) print_r($data);

                    printf("GeoSiret [suite] : %d etablissements géo-encodés (%d adresses non conformes), %d items/request, csv-size: %3.1fKo...\n", $this->done, $this->ko, min($this->done,$geo_items), filesize($csv) / 1024);

                    // Req. OK, on peut augmenter le rythme
                    $geo_items = min(API_ADRESSE_MAX_CSV_LINES, ceil($geo_items * 1.20 )) ;

                }

                unlink($csv);

                // Tempo si nécessaire (30 appels à l'API par seconde)
                while ( $this->apicalls > API_ADRESSE_MAXREQ_PER_PERIOD && $this->apicalls_by_s > API_ADRESSE_MAXREQ_PER_PERIOD/API_ADRESSE_PERIOD ) {
                    sleep (API_ADRESSE_SLEEPTIME);
                    $this->apicalls_by_s = $this->apicalls / (microtime(true) - $this->apitimestart);
                }

            }

        }


    }


// Class
}

?>