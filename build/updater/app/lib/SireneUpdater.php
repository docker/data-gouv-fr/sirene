<?php

require_once (__DIR__."/functions.php");
require_once (__DIR__."/callAPI.php");

class SireneUpdater
{

    public $_db;
    public $apiInfos;
    public $apiStatus;

    public $apicalls=0;
    public $apitimestart;
    public $apicalls_by_s=1;

    public $siren;
    public $siret;
    public $succession;
    public $APIAdresse;

    public function __construct($db = null) {

        $this->apitimestart=microtime(true);

        // DB
        if ( $db === null) {
            throw new \Exception("DB indisponible.\n" );

        } else {
            $this->_db = $db;
        }


        $this->apiInfos = $this->getInfos();

        // Test si service indispo
        if ( $this->apiInfos->httpcode == 404 ) {
            throw new \Exception("API indisponible.\n" . json_encode($this->apiInfos, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));
        }

        // Test si service disponible
        if ( $this->apiInfos->data->etatService != 'UP' ) {
            throw new \Exception("Service indisponible.\n" . json_encode($this->apiInfos->data, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));
        }

        // Récupération des états sur les différente endpoints
        $this->apiStatus = array();

        $svc=array();
        foreach ($this->apiInfos->data->etatsDesServices as $svc) {
            $this->apiStatus[$svc->Collection]["etat"] = $svc->etatCollection;
        }

        // Récup des dernières dates de traitement UL + Etab sur l'API.
        foreach ($this->apiInfos->data->datesDernieresMisesAJourDesDonnees as $col) {

            switch ( $col->collection ) {

                case "Unités Légales":
                case "Établissements":
                case "Liens de succession":
                    if ( $this->apiStatus[$col->collection]["etat"] == "UP" ) {

                        $this->apiStatus[$col->collection]["last_update"] = $col->dateDernierTraitementMaximum;

                    } else {

                        throw new \Exception("Service " . $col->collection . " indisponible.\n" . json_encode($this->apiStatus, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));

                    }

                    break;

            }

        }
        unset ($svc);

        // Instanciation des sous-classes
        if ( $this->apiStatus["Unités Légales"]["etat"] == "UP"  ) {

            $this->siren   = new Siren($this->_db);

        }

        if ( $this->apiStatus["Établissements"]["etat"] == "UP"  ) {

            $this->siret   = new Siret($this->_db);
        }

        if ( $this->apiStatus["Liens de succession"]["etat"] == "UP"  ) {

            $this->succession = new Succession($this->_db);

        }


        // On signale notre version à l'api-server
        if (UPDATER_SUBMIT_VERSION_TO_API !== false) {
            $headers = ["Content-Type: application/json"];
            $response = CallAPI('PUT',  API_STATUS_URL . "/updater", $headers ,
                                    json_encode(array(
                                            "component" => "updater",
                                            "version"   => UPDATER_VERSION
                                        )
                                    )
                                ) ;
            if ( $response->httpcode != 200 )
                printf(MY_NAME. "[WARNING] Code HTTP == " . $response->httpcode . " lors de la requête [PUT] " . API_STATUS_URL . "/updater\n");
        }
    }



    public function __destruct() {

        // Free resources
        if ( $this->apiStatus["Unités Légales"]["etat"] == "UP"  ) {
            unset($this->siren);
            unset($this->ndsiren);
        }

        if ( $this->apiStatus["Établissements"]["etat"] == "UP"  ) {
            unset($this->siret);
            unset($this->ndsiret);
        }

        if ( $this->apiStatus["Liens de succession"]["etat"] == "UP"  ) {
            unset($this->succession);
        }

    }



    public function sleep()
    {
        // Tempo si nécessaire (30 appels à l'API / 60 secondes)
        while ( $this->apicalls > API_INSEE_MAXREQ_BURST && $this->apicalls_by_s > API_INSEE_MAXREQ_PER_PERIOD/API_INSEE_PERIOD ) {
            sleep (API_INSEE_SLEEPTIME);
            $this->apicalls_by_s = $this->apicalls / (microtime(true) - $this->apitimestart);
        }
    }



    function getInfos() {

        $headers = [
            "Accept: application/json",
            "X-INSEE-Api-Key-Integration: ". API_INSEE_APIKEY
        ];

        $response = CallAPI('GET', API_INSEE_URL . API_INSEE_VERSION ."/informations" ,
                            $headers
        ) ;
        $this->apicalls++;

        switch ( $response->httpcode ) {
            case 200:
            case 404:
                return $response;
                break;

            default:
                throw new \Exception("Erreur lors de la requete vers l'API.\n" . json_encode($response, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));

        }

    }


    // Retourne, pour une classe (siret,siren) la date Max de dernier traitement (prise dans la table status).
    // - si on en a pas déjà une, on la recherche dans la DB (désolé -> il faut bien le faire une fois).
    public function getMaxLastUpdate($classe) {

        $sql = "select date_dernier_traitement as max from status where table_name='" . $this->$classe->viewname . "'";

        $res = $this->_db->query($sql);

        if ( $res !== false ) {
            if ( isset($res['max']) && strlen($res['max']) != 0 ) {

                return $res['max'];

            } else {

                return;

            }

        };

    }


    /*
     * synchro : synchro des établissements (siret) / unites légales (siren)
     *           - entre deux dates : $start ($date1) et $end ($date2)
    */

    function synchro( $classe, $d2, $step = 3600) {

        cli_set_process_title("sirene-updater: updating " . $classe);
        $synchrostart=microtime(true);

        // dates de départ possibles : MaxLastUpdate ou à défaut UPDATER_START_DATE (dev notamment)
        $format = "Y-m-d H:i:s.v";
        $dMLU = $this->getMaxLastUpdate($classe);

        $d1 = max( $dMLU, UPDATER_START_DATE) ;
        if (! str_contains($d1, ".")) $d1 .= ".0";

        printf(MY_NAME. "[%s] Démarrage: dernière donnée synchronisée: %s, Synchronisation depuis %s\n", ucfirst($classe), $dMLU, $d1);

        // Séparateur ' ' entre date et heure dans la DB
        if ( $dMLU === null ) {
          $date1 = date_create_from_format($format, UPDATER_START_DATE);
          $date2 = date_create_from_format($format, date($format) );
          $step  = 86400 * 30; // 1 mois si 1ère synchro

        } else {

            $tmp=explode(' ', $d1); $date1 = date_create_from_format($format, $tmp[0] . " " . $tmp[1]);
            // Séparateur 'T' dans l'API entre date et heure
            // 2024-04-05 : changement dans les dates fournies par /informations : ajout des décimales (.v)
            $tmp=explode('T', $d2); $date2 = date_create_from_format($format, $tmp[0] . " " . $tmp[1]);

        }


        // Boucle sur les steps écoulés depuis (heure par heure pour permettre un tri en conservant les perfs)
        $itemsread = $totalupdated = 0;

        $i1 = clone $date1;
        $i2 = clone $date1;

        $time_api = $time_db = 0;

        while ($i2 < $date2) {

            // On passe à l'intervalle suivant
            $i1 = clone $i2;

            // Fin = $step (1 heure) après début
            date_add($i2, date_interval_create_from_date_string($step . ' seconds') );

            // On passe inter_date au format date de l'API
            $inter1 = date_format($i1, 'Y-m-d\TH:i:s.v');
            $inter2 = date_format($i2, 'Y-m-d\TH:i:s.v');

            $str = sprintf(MY_NAME. "[%s] [%s, req=%0d, %1.3f req/s] period=%s-->%s", ucfirst($classe),
                                                            gmdate("H:i:s", ceil( microtime(true) - $synchrostart) ),
                                                            $this->apicalls,
                                                            $this->apicalls_by_s,
                                                            $inter1, $inter2);

            // updateItems : Get(API) and update(DB)
            $ts_start = microtime(true);

            $data = $this->updateItems( $classe, $inter1, $inter2 );

            $time_api     = $data['apitime'];
            $time_db      = $data['dbtime'];
            $itemsread    += $data['itemsread'];
            $totalupdated += $data['updated'];


            // Constitution et affichage des métriques
            $str.=sprintf(", Time: %1.2fs", microtime(true) - $ts_start);
            $str.=sprintf(", API: %d;%1.2fs", $data['itemsread'], $time_api);
            if ( $data['itemsread'] != 0 ) $str.=sprintf(";%01.2f items/s", $data['itemsread'] / $time_api);
            $str.=sprintf(", DB: %d;%01.2fs",  $data['updated'], $time_db);
            if ($data['updated'] != 0 ) $str.=sprintf(";%01.2f upd/s", $data['updated'] / $time_db);

            printf("%s, MEM: %01.2fMo (peak=%01.2fMo)\n" , $str ,  memory_get_usage()/1024/1024, memory_get_peak_usage()/1024/1024 );

            // Free mem
            unset ($data);
            gc_collect_cycles();

            // Update status
            $this->_db->updateStatus($this->$classe->viewname, "date_dernier_traitement", max($dMLU, $this->$classe->dernier_traitement) );

        }

        // (status) Maj de date de last_update si on a fait des updates
        if ( $totalupdated != 0 ) {
            $this->_db->updateStatus($this->$classe->viewname, "last_update", gmdate('D, d M Y H:i:s T', time()) );
        }

        // Synchro terminée, on met à jour last_sync
        $this->_db->updateStatus($this->$classe->viewname, "last_sync", gmdate('D, d M Y H:i:s T', time()) ); // RFC-1123


        // Comptage si la dernière MAJ a été modifiée
        if ($this->$classe->dernier_traitement !== null) {
             $this->_db->updateCount($this->$classe->viewname);
        }

        $msg = sprintf(MY_NAME. "[%s] Fin de traitement: Items (r/w): %d/%d. Durée %s. Mémoire (peak) utilisée: %01.2fM.",
                ucfirst($classe),
                                                $itemsread, $totalupdated,
                                                            gmdate("H:i:s", ceil (time() - $synchrostart)),
                                                                        memory_get_peak_usage()/1024/1024);

        if ( $this->_db->rows !== null ) {
            $msg .= " Rows: " . $this->_db->rows;
        }
        printf($msg . "\n");

    }

    // Stockage d'un item après éventuel enrichissement
    function deleteItem($classe, $item) {

        $sql = "DELETE FROM " . $this->$classe->viewname . " WHERE " . $classe . "='" . $item . "'";

        $res = $this->_db->query($sql);


    }
    // Stockage d'un item après éventuel enrichissement
    function storeItem($classe, $item, $delete_before = false) {


        // "siret" : On geocode au passage (voir Siret/geoCode)
        if ($classe == "siret" ) {

            // Geocodage à la volée
            if ( UPDATER_GEOCODE_DURING_SIRET_SYNC !== false ) {

                // Pour le geocodage à la volée
                $this->APIAdresse = new APIAdresse();

                cli_set_process_title("sirene-updater: geo-encoding siret " . $item->siret);

                $adresse = $this->$classe->getAdresse($item->adresseEtablissement);

                // On ne sollicite l'API adresse que si on a une adresse sur cet établissement
                if ( $adresse !== false ) {

                    $geoinfos = $this->APIAdresse->getGeoInfos( $adresse );

                    // En retour, on a un tableau, avec forcément à minima un geo_score à -999 si pas de réponse
                    // et des données si on a trouvé une adresse.
                    if ( is_array($geoinfos) ) {
                        foreach ($geoinfos  as $key => $val) {
                            $item->$key = $val;
                        }
                        $this->APIAdresse->ok_calls++;
                    }

                    // On a eu une autre erreur HTTP, on logue.
                    else
                    {
                        printf("\nAPI-Adresse : ERROR. Siret: %s ==> %s\n", $item->siret, $geoinfos);
                        $item->geoScore = -999;
                        $this->APIAdresse->ko_calls++;
                    }
                }
                // Pour le geocodage à la volée
                unset($this->APIAdresse);
            }

            else // Geocodage plus tard, on donne un geo_score spécifique.
            {
                $item->geoScore = 0;
            }

        }

        // On stocke et on en profite pour faire maintenant le trt uppercase_to_underscore
        $rc = $this->$classe->DBupdate($item);

        if ( $rc === false) {

            throw new \Exception("Erreur lors de la requete storeItem en DB.\n" . json_encode($item, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));

        } else {

            // Update status each update
            $this->_db->updateStatus($this->$classe->viewname, "date_dernier_traitement", $this->$classe->dernier_traitement );
            $rc = "done";
        }

        return $rc;

    }


    // Récupération d'un tableau avec toutes les entités (ul, et, ...) modifiées dans une plage de dates
    // 2021-12 : passage en mode curseur
    function updateItems( $classe, $startdate = null, $enddate = null) {

        $data = array(
            "itemsread" => 0,
            "apitime" => 0,
            "updated" => 0,
            "dbtime" => 0
        );

        // Plage de dates
        if ( is_null($startdate) ) {
            $month = date("Y-m");
            $startdate = $month . "-01T00:00:00" ;
        }

        if ( is_null($enddate) ) {
            $enddate = date("Y-m-d\TH:i:s");
        }

        $headers = [
            "Accept: application/json",
            "X-INSEE-Api-Key-Integration: ". API_INSEE_APIKEY
        ];

        // TRI : https://www.sirene.fr/static-resources/doc/INSEE_Documentation-Extrait_tri.pdf
        // Curseurs : https://www.sirene.fr/static-resources/doc/INSEE_Documentation-Extrait_curseurs.pdf
        $curseur = "*";


        while (true) {

            $total = 0;

            $ts_start = microtime(true);

            cli_set_process_title("sirene-updater: fetching data for " . $classe);

            switch ($classe) {

                case "siren":
                    $response = CallAPI( 'GET',
                         API_INSEE_URL . API_INSEE_VERSION . "/siren?" .
                        "q=" .
                        "dateDernierTraitementUniteLegale" . ":%7B" . $startdate . "%20TO%20". $enddate ."%5D" .
                        "&masquerValeursNulles=true" .
                        "&tri=dateDernierTraitementUniteLegale" .
                        "&curseur=" . $curseur .
                        "&nombre=" . API_INSEE_MAX_ITEMS_PER_REQUEST,
                        $headers
                    ) ;

                    $itemName = "unitesLegales";
                    break;


                case "siret":
                    $response = CallAPI( 'GET',
                        API_INSEE_URL . API_INSEE_VERSION . "/siret?" .
                        "q=" .
                        "dateDernierTraitementEtablissement" . ":%7B" . $startdate . "%20TO%20". $enddate ."%5D" .
                        "&masquerValeursNulles=true" .
                        "&tri=dateDernierTraitementEtablissement" .
                        "&curseur=" . $curseur.
                        "&nombre=" . API_INSEE_MAX_ITEMS_PER_REQUEST,
                        $headers
                    ) ;

                    $itemName = "etablissements";
                    break;


                case "succession":
                    $response = CallAPI( 'GET',
                        API_INSEE_URL . API_INSEE_VERSION  . "/siret/liensSuccession?" .
                        "q=" .
                        "dateDernierTraitementLienSuccession" . ":%7B" . $startdate . "%20TO%20". $enddate ."%5D" .
                        "&curseur=" . $curseur.
                        "&nombre=" . API_INSEE_MAX_ITEMS_PER_REQUEST,
                        $headers
                    ) ;

                    $itemName = "liensSuccession";
                    break;

                default:
                    throw new \Exception("UpdateItems: Erreur de classe inexistante: " . $classe);

            }

            // On compte (et on logguera) tous les appels à l'API déjà réalisés
            $this->apicalls++;

            $this->apicalls_by_s = $this->apicalls / (microtime(true) - $this->apitimestart);
            $data['apitime'] += microtime(true) - $ts_start;

            // Test du CR : on risque la 404 si aucun élément trouvé.
            switch ( $response->httpcode ) {

                case "200":  // On a des données...

                    $total = $response->data->header->total;
                    $ts_start = microtime(true);

                    foreach ($response->data->$itemName as $item) {

                        $rc = $this->storeItem($classe, $item);

                        if ( $rc === false) {

                            throw new \Exception("Erreur lors de l'appel à storeItem.\n" . json_encode($item, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));

                        } else {

                            $data['updated']++;

                            // Update status each update
                            $this->_db->updateStatus($this->$classe->viewname, "date_dernier_traitement", $this->$classe->dernier_traitement );

                        }


                        if ( memory_get_usage()/1024/1024 > MAX_MEM_FOR_RS_LOAD ) {
                            throw new \Exception("Surcharge memoire : ". sprintf("%01.2f Mo. ", memory_get_usage()/1024/1024) . "Reduire la valeur de UPDATER_TIME_INTERVAL.\n") ;
                        }

                    };
                    $data['dbtime'] += microtime(true) - $ts_start;

                    break;

                case "429": // On a été trop vite...
                    printf (" [HTTP-429, tempo %d s]...", API_INSEE_SLEEPTIME);
                    cli_set_process_title("sirene-updater: sleeping due to HTTP-429");

                    $this->sleep();
                    $total = 0;
                    $data['updated'] = 0;
                    $data['dbtime'] = 0;
                    break;

                case "404": // Pas de données, on ne fait rien, mais ce n'est pas une erreur.
                    break;

                default:
                    throw new \Exception("Erreur lors de la requete HTTP.\n" . json_encode($response, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));

            }
            $data['itemsread'] = $total;


            // Si réponse et si on a rempli le buffer, on se prépare à récupérer la suite, sinon, on breake
            if ($total != 0 && API_INSEE_MAX_ITEMS_PER_REQUEST == $response->data->header->nombre) {

                $curseurSuivant=$response->data->header->curseurSuivant;

                // Si le curseur est inchangé, on a fini. Sinon, on avance le curseur
                if ( urldecode($curseur) == $curseurSuivant ) {

                    break;
                }
                else
                {
                    $curseur = urlencode($curseurSuivant);

                }

            } else {

                break;

            }

            unset ($response);

            // Tempo si nécessaire (30 appels à l'API / 60 secondes)
            $this->sleep();
        }

        $this->sleep();
        return $data;

    }



    // Requête sur un item en particulier
    /*
     * Retourne une chaine de caractières (si erreur) ou un tableau (si on a des données)
     */
    function getItem( $classe, $id = null) {


        // On a besoin d'un ID numérique
        if ($id === null ) return "Is-Null";
        if (! is_numeric($id) ) return "Is-Not-Num";

        // Contrôle des ID
        // contrôle de l'item : siret/siren + longueur et type
        // --> éviter des appels BAD request à l'API Insee
        switch ( $classe ) {

            case "siret":
            case "ndsiret":
                if ( strlen($id) != 14 ) {
                    return "Bad-Id";
                }
                break;

            case "siren":
            case "ndsiren":
                if ( strlen($id) != 9 ) {
                    return "Bad-Id";
                }
                break;

            default:
                return "Bad-Type";
        }


        $headers = [
            "Accept: application/json",
            "X-INSEE-Api-Key-Integration: ". API_INSEE_APIKEY
        ];

        switch ($classe) {

            case "siren":
                $itemName = "uniteLegale";
                $response = CallAPI( 'GET',
                            API_INSEE_URL . API_INSEE_VERSION  . "/siren/" . $id . "?masquerValeursNulles=true",
                            $headers
                            );
                $this->apicalls++;

                while ( $response->httpcode == 301 ) {
                    // On a une redirection... ex. "enregistrements en doublons" (ex. siren/833666449)
                    // Le siren cible de la redirection est donné dans le message :
                    // ==> "Unité légale doublon, redirection vers l'unité légale : 795349604"

                    if (preg_match("/doublon, redirection vers/", $response->data->header->message)) {

                        $redirect_id = preg_replace("/.*doublon, redirection vers.* : /", "", $response->data->header->message);

                        echo " Redirection vers $redirect_id...";
                        $redirection = true;
                        $response = CallAPI( 'GET',
                                            API_INSEE_URL . API_INSEE_VERSION  . "/siren/" . $redirect_id . "?masquerValeursNulles=true",
                                            $headers
                                            );

                        // On est au bout du jeu de piste ?
                        if ($response->httpcode == 200) {
                            $response->data->$itemName->siren = $id;
                        }

                        $this->apicalls++;
                        $this->sleep();

                    }

                }

                break;


            case "siret":

                $itemName = "etablissement";
                $response = CallAPI( 'GET',
                    API_INSEE_URL . API_INSEE_VERSION  . "/siret/" . $id . "?masquerValeursNulles=true",
                    $headers
                );
                $this->apicalls++;

                while ( $response->httpcode == 301 ) {
                    // Cas des redirections :
                    // ex siret/78434391500012 -> redirige vers un SIREN... 839008166, dont il faut retrouver le SIRET du siege...

                    if (preg_match("/doublon, redirection vers/", $response->data->header->message)) {

                        $redirect_siren = preg_replace("/.*doublon, redirection vers.* : /", "", $response->data->header->message);
                        $redirect_nic_siege = $this->getItem("siren", $redirect_siren)->periodesUniteLegale[0]->nicSiegeUniteLegale;
                        $redirect_id = $redirect_siren . $redirect_nic_siege;
                        echo "Redirection vers $redirect_id...";

                        $redirection = true;
                        $response = CallAPI( 'GET',
                                            API_INSEE_URL . API_INSEE_VERSION  . "/siret/" . $redirect_id . "?masquerValeursNulles=true",
                                            $headers
                                            );
                        // On est au bout du jeu de piste ?
                        if ($response->httpcode == 200) {
                            $response->data->$itemName->siret = $id;
                        }

                        $this->apicalls++;
                        $this->sleep();

                    }

                }
                break;


            default:
                throw new \Exception("getItem: Erreur de classe inexistante: " . $classe);

        }

        // On compte (et on logguera) tous les appels à l'API réalisés
        // $this->apicalls++;
        $this->apicalls_by_s = $this->apicalls / (microtime(true) - $this->apitimestart);


        // Test du CR : on risque la 404 si aucun élément trouvé.
        switch ( $response->httpcode ) {

            case "200":  // On a des données...
                // C'est un tableau que l'on retourne
                $data = $response->data->$itemName;
                break;

            case "403": // Non Diffusible / sable (ex. 20006679300023 ou 785937426)
                $data = "Non-Diff";
                break;

            case "429": // On a été trop vite...
                //printf(MY_NAME. "[Spool] [HTTP-429, tempo %d s].\n", API_INSEE_PERIOD);
                cli_set_process_title("sirene-updater: sleeping due to HTTP-429");
                $data = "HTTP-429";

                $this->sleep();
                break;

            case "404": // Pas de données, on ne fait rien, mais ce n'est pas une erreur.
                $data = "Not-Found";
                break;

            default:
                throw new \Exception("Erreur lors de la requete HTTP.\n" . json_encode($response, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT));

        }
        unset ($response);

        // Tempo si nécessaire (30 appels à l'API / 60 secondes)
        $this->sleep();

        return $data;

    }



// Class
}

?>