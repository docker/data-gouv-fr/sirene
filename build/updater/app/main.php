<?php

    /*

    Interrogation de l'API Insee Sirene / synchronisation.

    Nécessite une ApiKey, il faut donc créer des identifiants : 
        https://portail-api.insee.fr/
          Voir doc/2025-02-28_apim.md
    */
    require_once (__DIR__."/config/config.php");

    /*
     * Autoloader
     *
     */
    function loadClass($class)
    {
      require (__DIR__."/lib/" . $class . ".php");
    }
    spl_autoload_register('loadClass');

    // Active le GC
    gc_enable();

    // Nom du processus
    cli_set_process_title("sirene-updater");
    echo MY_NAME. " Démarrage...\n";

    // Au démarrage, on temporise un peu et on attend que la DB soit prête
    $db = false;
    cli_set_process_title("sirene-updater: waiting for db");
    while ( $db === false ) {

        echo MY_NAME. " En attente de la DB...\n";
        sleep (1);
        $db = new UpdaterDB();

        // On s'assure de la présence des tables techniques
        $db->checkTables();
        $db->checkTableColumn("spool", "reason", "varchar(16)");

    };
    echo MY_NAME. " La DB est prête...\n";

    // Temps de sommeil
    $sleeptime=0;

    // Init status
    $db->initStatus();

    // Main loop
    while (true)
    {

        // Db up ! on détermine sa couleur
        cli_set_process_title("sirene-updater: checking if DBinit is running...");

        while ( $db->getColor() == "gray" ) {

            echo "DB répond, mais DBinit est en cours. On attend...\n";
            sleep (600);

        }
        // echo MY_NAME. "DBinit ne tourne pas. On continue...\n";

        try {


            if ( is_null(API_INSEE_APIKEY)   || API_INSEE_APIKEY == "null" )    throw new Exception("Missing API Key (API_INSEE_APIKEY envvar)");


            // On ne joue la synchro globale que tous les WAKEUP_DELAY
            if (  (UPDATER_INSEE_SYNC == "true" ) && ($sleeptime % UPDATER_WAKEUP_DELAY) == 0 ) {

                cli_set_process_title("sirene-updater: full sync");
                echo MY_NAME. " Déclenchement de la synchronisation complète.\n";

                $updater = new SireneUpdater($db);

                // Synchro des unités légales
                $data = $updater->synchro( 'siren', $updater->apiStatus["Unités Légales"]["last_update"], UPDATER_TIME_INTERVAL );

                // Synchro des UL non diffusibles
                // inhibé 16/08/2023--> https://www.insee.fr/fr/information/6683782
                // $data = $updater->synchro( 'ndsiren', $updater->apiStatus["Unités Légales"]["last_update"], UPDATER_TIME_INTERVAL_ND );

                // Synchro des établissements
                $data = $updater->synchro( 'siret', $updater->apiStatus["Établissements"]["last_update"] , UPDATER_TIME_INTERVAL);

                // Synchro des ET non diffusibles
                // inhibé 16/08/2023--> https://www.insee.fr/fr/information/6683782
                // $data = $updater->synchro( 'ndsiret', $updater->apiStatus["Établissements"]["last_update"], UPDATER_TIME_INTERVAL_ND );

                // Synchro des liens de succession
                $data = $updater->synchro( 'succession', $updater->apiStatus["Liens de succession"]["last_update"], UPDATER_TIME_INTERVAL_SUCCESSION );

                /*
                *  Geolocalisation des items existants (BAN)
                */
                if ( API_ADRESSE_CSV_CALLS_ENABLED !== false ) {

                    cli_set_process_title("sirene-updater: geo-encoding");

                    $geo = new MassGeoSiret($db);
                    // printf("MassGeoSiret [Démarrage] : %d etablissements à géo-encoder...\n", $geo->todo);
                    if ( $geo->todo == 0 ) {

                        printf(MY_NAME. "[MassGeoSiret] aucun établissement restant à géo-encoder.\n");

                    }
                    else
                    {
                        printf(MY_NAME. "[MassGeoSiret] Démarrage: %d établissements à géo-encoder\n", $geo->todo);

                        $rc = $geo->geoCodage();

                        printf("[MassGeoSiret [Fin] : %d/%d etablissements géo-encodés (%d adresses non conformes).\n", $geo->done,  $geo->todo, $geo->ko);

                    }

                    // Nettoyage
                    unset($geo);

                }
                else
                {
                    printf(MY_NAME. "[MassGeoSiret] Désactivé (Env_var API_ADRESSE_CSV_CALLS_ENABLED = false / unset).\n");
                }

                // Le destructeur libère les sous-classes
                unset($updater);

            }





            // Traitement du spool de synchro (prioritaire)
            /*
             * Principe du spool :
             * - [x] si une requête GET retourne une erreur, on trace dans le spool
             * - [x] l'updater parcourt le spool pour vérifier auprès de Insee si on peut synchroniser la donnée.
             * - [x] si l'insee retourne un statut non-diffusible, marquage en tant que tel dans le spool
             * - [x] pour les ndsiret / ndsiren identifiés dans le spool, création enregistrement dans les tables
             */

            // Purge du spool / delai : UPDATER_SPOOL_PURGE_DELAY
            /*
             * On commence par la purge, si des erreurs se produisent plus tard, elles ne bloqueront pas longtemps
             */
            $db->pruneSpool();

            /*
                Parcours de la table "spool" à la recherche de siret / siren à vérifier
                $item = array(type, id)
                       type = 'siret' 'siren'
                       id = un-siret, ou un-siren

            */
            $spool = $db->getSpooledItems();

            if ($spool !== false) {

                // on consomme le spool s'il contient des éléments
                cli_set_process_title("sirene-updater: spool sync");
                printf (MY_NAME. "[Spool] Synchronisation des éléments en attente (%d).\n", count($spool));

                // On instancie l'updater
                $updater = new SireneUpdater($db);


                // On va compter les items, pour optimiser le stress de l'API Insee
                $n_items=0;

                foreach($spool as $item) {

                    printf(MY_NAME. "[Spool] [req=%0d, %1.3f req/s] %s/%s (%s) ... ", 
                                             $updater->apicalls, $updater->apicalls_by_s,
                                                                     $item['item_type'], $item['item_id'], $item['reason'] ?? "none");
                    $classe = $item['item_type'];


                    // On détruit l'item si le refresh est spoolé pour cause de "Statut Diffusion"
                    // if ( str_starts_with($item['reason'] ?? "none",  "Statut-Diff") )  {
                    //     printf("[ND] DELETED ");
                    //     $updater->deleteItem($classe, $item['item_id']);
                    // }

                    // On tente de recupérer l'info sur l'Insee
                    $data = $updater->getItem($classe, $item['item_id']);

                    // Si on a un objet, c'est qu'on a trouvé l'item
                    if (is_object($data)) {
                        $status = $updater->storeItem($classe, $data);

                    } else { // Sinon, on a eu un problème...

                        // Non dissufable ?
                        if ($data == "Non-Diff") {
                            $nd = new stdClass();
                            $nd->$classe = $item['item_id'];
                            $nd->statutDiffusion = "N";
                            $status = $updater->storeItem($classe, $nd);
                            unset ($nd);
                        }
                        else
                        {
                            $status = $data;
                        }
                    }

                    // Maj du spool
                    if ( $data != "HTTP-429") $db->updateSpooledItem($item , $status);

                    // Fin de traitement pour cet item
                    printf("$status\n");

                }
                printf(MY_NAME. "[Spool] Fin.\n");

                // Le destructeur libère les sous-classes
                unset($updater);

            }
            // else {
            //     echo MY_NAME. "Pas d'élément en attente dans le spool.\n";
            // }

            unset($spool);







        } catch (Exception $e) {
            echo 'Exception reçue : ',  $e->getMessage(), "\n";
            printf("[Statut Memoire: %01.2fMo, Max(peak): %01.2fMo]\n", memory_get_usage()/1024/1024, memory_get_peak_usage()/1024/1024);

        }

        // Et enfin... soit on s'arrête, soit on s'endort
        if (UPDATER_ONESHOT_RUN == "true") {
            cli_set_process_title("sirene-updater: exiting");
            echo MY_NAME. "mode Oneshot... Arrêt immédiat !\n" ;break;
        }

        // Sommeil
        // echo MY_NAME. "En sommeil pour " . UPDATER_LOOP_DELAY . " secondes.\n";
        cli_set_process_title("sirene-updater: sleeping");
        $sleeptime+=UPDATER_LOOP_DELAY;
        sleep(UPDATER_LOOP_DELAY);

    }

    unset ($db);

?>
