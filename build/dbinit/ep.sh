#!/bin/bash

export PGHOST=${POSTGRES_DB_HOST:-db}
export PGPORT=${POSTGRES_DB_PORT:-5432}
export PGUSER=${POSTGRES_DB_USER:-sirene_user}
export PGPASSWORD=${POSTGRES_DB_PASSWORD:-sirene_password}
export PGDATABASE=${POSTGRES_DB_NAME:-sirene}
export DATE=$(date +%Y%m01)

# - Fonction chronopsql utilisée dans les enrichissements
# ------------
chronopsql() {
  OUT=/tmp/chronopsql.out
  DD=$(date +%s)
  psql -U ${PGUSER} -d ${PGDATABASE} -c "$@" >${OUT}

  [ $? -ne 0 ] && RETURN_CODE=1
  DF=$(date +%s)
  T=$(( DF - DD ))

  # compta
  if grep -qE "^UPDATE|^COPY" ${OUT} ; then
      OP=$(cut -d" " -f1 ${OUT})
      NR=$(cut -d" " -f2 ${OUT})
      [ "${T}" == 0 ] && T=1
      echo "${OP} ${NR} records in ${T} seconds, $(( NR / T )) records/second."
  else
      cat ${OUT}
  fi

  return ${RETURN_CODE:-0}
}

export -f chronopsql
# ------------------

get_table_color() {

    # Guess if DB is blue or green
    RES=$(psql -U ${PGUSER} -d ${PGDATABASE} -t -c "
    SELECT EXISTS (
        SELECT FROM information_schema.tables
        WHERE  table_schema = 'public'
        AND    table_name   = '${1}_blue'
    );
    ")

    # Attention à l'espace devant le "f" ou le "t"
    if [ "${RES// }" == "t" ] ; then
        # Blue est là, on déploie GREEN
        export NEW_COLOR=green
        export OLD_COLOR=blue
    else
        # Blue n'est pas là, on déploie BLUE
        export NEW_COLOR=blue
        export OLD_COLOR=green
    fi

}

export -f get_table_color
# ------------------


# On attend que le DL soit terminé
while sleep 15 ; do
    [ ! -f /downloads/download.lock ] && break
    echo "Download lock present, waiting..."
done
[ ! -f /downloads/download-done ] && echo "Manque le fichier 'download-done' permettant de démarrer l'import. Abandon." && exit 1


echo "DBINIT (${VERSION:-0.0}): Init/Upgrade de la db..."

# Exécution des scripts de l'EP
for S in /ep.d/00-before/*.sh ; do "$S" ; done

# RNA
get_table_color rna
echo "DBINIT: passage de RNA en ${NEW_COLOR}"
if [ ${DO_RNA:-true} == "true" ] ; then
  for S in /ep.d/10-rna/*.sh ; do 
    "$S"
    [ $? != 0 ] && echo "Erreur sur le script $S : abandon." && exit 1
  done
fi

# Sirene
get_table_color unite_legale
echo "DBINIT: passage de Sirene en ${NEW_COLOR}"
if [ ${DO_SIRENE:-true} == "true" ] ; then
  for S in /ep.d/20-sirene/*.sh ; do 
    "$S" 
    [ $? != 0 ] && echo "Erreur sur le script $S : abandon." && exit 1
  done
fi

# POST
for S in /ep.d/99-after/*.sh ; do "$S" ; done

echo "DBINIT-OK: Init/Upgrade Done !"
