#!/bin/bash


TABLE_NAME=etablissement_${NEW_COLOR}

echo -n "$0 : DROP TABLE ${TABLE_NAME}... "
chronopsql "DROP TABLE IF EXISTS ${TABLE_NAME} CASCADE;"

echo -n "$0 : CREATE TABLE ${TABLE_NAME}... "


chronopsql "CREATE TABLE ${TABLE_NAME} as SELECT
E.*
FROM tmp_etablissement E;
"


echo -n "$0 : Drop temporary table etablissement... "
chronopsql "DROP TABLE tmp_etablissement;"


# Clé primaire
echo -n "$0 : Primary key ${TABLE_NAME}(siret)... "
echo ""
chronopsql "ALTER TABLE ${TABLE_NAME} ADD PRIMARY KEY(siret);"

# Ajout des colonnes geo
echo "$0 : Ajout des 9 colonnes geo_*... "
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN longitude REAL default NULL;"
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN latitude  REAL default NULL;"
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN geo_score  DECIMAL(9,2) default NULL;"
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN geo_type CHARACTER VARYING default NULL;"
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN geo_adresse CHARACTER VARYING default NULL;"
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN geo_id CHARACTER VARYING default NULL;"
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN geo_ligne CHARACTER VARYING default NULL;"
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN geo_l4 CHARACTER VARYING default NULL;"
chronopsql "ALTER TABLE ${TABLE_NAME} ADD COLUMN geo_l5 CHARACTER VARYING default NULL;"

