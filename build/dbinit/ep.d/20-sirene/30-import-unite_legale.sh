#!/bin/bash

# URL : https://files.data.gouv.fr/insee-sirene/StockUniteLegale_utf8.zip

# Création Table avec ajout d'une colonne ID systématiquement
# Import des données

# Fichier déjà atendu présent.
FNAME=StockUniteLegale_utf8.zip

# On travaille dans le volume persistant
cd /downloads
rm -f *.csv


# On attend que le DL soit fait.
if [ -f "${FNAME/zip/csv}.lock" ]; then
    echo -n "Lock présent, attente du téléchargement"
    while [ -f "${FNAME/zip/csv}.lock" ]; do
        echo -n "." && sleep 2
    done
fi


# Schéma de données
# Unite_legale uniquement
TABLE_NAME=unite_legale_${NEW_COLOR}

# Nom des colonnes est convertit de camelCase à underscore_case
DATATYPES='
siren VARCHAR(9),
statut_diffusion CHARACTER VARYING,
unite_purgee CHARACTER VARYING,
date_creation  date default NULL,
sigle CHARACTER VARYING,
sexe CHARACTER VARYING,
prenom_1 CHARACTER VARYING,
prenom_2 CHARACTER VARYING,
prenom_3 CHARACTER VARYING,
prenom_4 CHARACTER VARYING,
prenom_usuel CHARACTER VARYING,
pseudonyme CHARACTER VARYING,
identifiant_association CHARACTER VARYING,
tranche_effectifs CHARACTER VARYING,
annee_effectifs CHARACTER VARYING,
date_dernier_traitement TIMESTAMP default NULL,
nombre_periodes SMALLINT,
categorie_entreprise CHARACTER VARYING,
annee_categorie_entreprise CHARACTER VARYING,
date_debut date default NULL,
etat_administratif CHARACTER VARYING,
nom CHARACTER VARYING,
nom_usage CHARACTER VARYING,
denomination CHARACTER VARYING,
denomination_usuelle_1 CHARACTER VARYING,
denomination_usuelle_2 CHARACTER VARYING,
denomination_usuelle_3 CHARACTER VARYING,
categorie_juridique CHARACTER VARYING,
activite_principale CHARACTER VARYING,
nomenclature_activite_principale CHARACTER VARYING,
nic_siege CHARACTER VARYING,
economie_sociale_solidaire CHARACTER VARYING,
societe_mission CHARACTER VARYING,
caractere_employeur CHARACTER VARYING,
'

# SQL="CREATE UNLOGGED TABLE ${TABLE_NAME} (id SERIAL NOT NULL,"
SQL="CREATE TABLE ${TABLE_NAME} (id SERIAL NOT NULL,"
COLS=""

while read COL ; do

    # On ignore les lignes commentées et celles vides
    [ "${COL:0:1}" = "#" ] && continue
    [ -z "${COL}" ] && continue

    COL_NAME=${COL# *} ; COL_NAME=${COL_NAME%% *}
    COL_TYPE=${COL#*${COL_NAME} }

    COLS="${COLS} ${COL_NAME},"
    SQL="${SQL} ${COL_NAME} ${COL_TYPE}"

done <<< ${DATATYPES}

# Ajout des colonnes timestamps (pas concernées dans le CSV)
SQL="${SQL}
date_fin   date default NULL,
created_at TIMESTAMP default (now() at time zone 'utc'),
updated_at TIMESTAMP default (now() at time zone 'utc'),
"

# Retrait de la dernière virgule
SQL="${SQL%,*} );"
COLS="${COLS%,}"

echo -n "Création de la table: "
chronopsql "DROP TABLE IF EXISTS ${TABLE_NAME} CASCADE;"
chronopsql "${SQL}"
chronopsql "\d ${TABLE_NAME}"


# Extraction uniquement si on a pas déjà le CSV du même mois.
TS_ZIP=$(date +%Y%m01 --date=@$(stat -c %Y ${FNAME}))

if [ "${DEVMODE:-false}" = false ] \
&& [ -f "${FNAME/zip/csv}" ] &&  TS_CSV=$(date +%Y%m01 --date=@$(stat -c %Y ${FNAME/zip/csv})) \
&& [  "${TS_CSV:-0}" == "${TS_ZIP}"  ] ; then

      echo "${FNAME/zip/csv} déjà extrait et du même mois que le zip."

else

   echo "$0: Extration de ${FNAME}... "
   # Avec quelques substitutions (NÂ°1 -> N°1) : 's,\xc2\xb0,°,'
   [ -f ${FNAME} ] && unzip -qc ${FNAME} | sed -e 's,\xc2\xb0,°,g' | head -n 1        >  ${FNAME/zip/csv}

   # Calcul NF, pour valider la longueur de chaque ligne retenue (erreur sur siren 998813901 dans stockUL 04/2024)
   NF=$( awk -F, '{ print NF}' < ${FNAME/zip/csv})

   # DEVMODE : filtre sur "activite_principale": "DEVMODE_UL_FILTRE"

   if [ "${DEVMODE:-false}" = true ]; then
      FILTRE="${DEVMODE_UL_FILTRE:-62.02A}"
      echo "$0: DEVMODE, filtre sur les activités : ${FILTRE}"
      [ -f ${FNAME} ] && unzip -qc ${FNAME} | grep "${FILTRE}" >> ${FNAME/zip/csv}

      cp ${FNAME/zip/csv} ${FNAME/zip/csv}.debug

   else

      [ -f ${FNAME} ] && unzip -qc ${FNAME} | tail -n +2       >> ${FNAME/zip/csv}

   fi

   echo "$0: Extraction OK."

fi

# On s'assure qu'il y a un line feed au bout
echo "" >> ${FNAME/zip/csv}


# Import dans la DB
echo -n "Import du CSV... ($NF champs) "
grep -v "^ *$" ${FNAME/zip/csv} | chronopsql "\copy ${TABLE_NAME}(${COLS}) FROM STDIN delimiter ',' CSV HEADER ENCODING 'UTF8';"

# Suppression du CSV
rm -f ${FNAME/zip/csv}

# Clé primaire
echo -n "$0 : ${TABLE_NAME} (siren)... "
chronopsql "ALTER TABLE ${TABLE_NAME} ADD PRIMARY KEY(siren);"
