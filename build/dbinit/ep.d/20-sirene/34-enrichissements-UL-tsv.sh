#!/bin/bash
# Enrichissement des données

exit

echo -n "$0: Enrichissement unite_legale_${NEW_COLOR} : Add TSV columns... "
chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ADD COLUMN tsv tsvector, ADD COLUMN tsv_nomentreprise tsvector, ADD COLUMN tsv_enseigne tsvector;"
#chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ADD COLUMN tsv tsvector;"
#chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ADD COLUMN tsv_nomentreprise tsvector;"
#chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ADD COLUMN tsv_enseigne tsvector;"

echo -n "$0: Enrichissement unite_legale_${NEW_COLOR} : Update tsv_nomentreprise... "
chronopsql "
UPDATE unite_legale_${NEW_COLOR} S
      SET tsv_nomentreprise =
          setweight(to_tsvector(coalesce(sigle,'')), 'A') ||
          setweight(to_tsvector(coalesce(denomination,'')), 'B');
"

echo -n "$0: Enrichissement unite_legale_${NEW_COLOR} : Index TSV nomentreprise... "
chronopsql "CREATE INDEX tsv_nomentreprise_${NEW_COLOR} ON unite_legale_${NEW_COLOR} USING gin(tsv_nomentreprise);"

