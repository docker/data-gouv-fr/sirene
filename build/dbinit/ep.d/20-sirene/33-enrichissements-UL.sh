#!/bin/bash
# Enrichissement des données


DECOUPE=$(echo {00..99})

echo -n "$0: Enrichissement unite_legale_${NEW_COLOR} : add column enseignes... "
chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ADD COLUMN enseignes TEXT;"

echo -n "$0: Enrichissement unite_legale_${NEW_COLOR} : add column etablissements... "
chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ADD COLUMN etablissements TEXT;"

echo -n "$0: Enrichissement unite_legale_${NEW_COLOR} : add column nombre etablissements... "
chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ADD COLUMN nombre_etablissements INTEGER;"

echo -n "$0: Enrichissement unite_legale_${NEW_COLOR} : add column numéro de TVA... "
chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ADD COLUMN numero_tva_intra TEXT;"



for SIREN_PREFIX in ${DECOUPE} ; do

    echo -e "\n$0: Enrichissement unite_legale_${NEW_COLOR} débutant par ${SIREN_PREFIX}"

    echo -n "    + enseignes..."
    chronopsql "
    UPDATE unite_legale_${NEW_COLOR} S
    SET enseignes =
    (SELECT STRING_AGG(enseignes,', ') AS enseignes
        FROM (select DISTINCT ST.enseigne_1 as enseignes
            from etablissement_${NEW_COLOR} ST
            where ST.siren = S.siren AND ST.enseigne_1 IS NOT NULL
            ) tbl
        )
    WHERE siren like '${SIREN_PREFIX}%';
    "

    echo -n "    + etablissements..."
    chronopsql "
    UPDATE unite_legale_${NEW_COLOR} S
    SET etablissements =
    (SELECT STRING_AGG (siret, ',') AS etablissements
       FROM (SELECT siret FROM etablissement_${NEW_COLOR} where siren = S.siren LIMIT 10) tbl)
    WHERE siren like '${SIREN_PREFIX}%';
    "

    echo -n "    + nombre_etablissements..."
    chronopsql "
    UPDATE unite_legale_${NEW_COLOR} S
    SET nombre_etablissements = (SELECT COUNT(*) AS nombre_etablissements from etablissement_${NEW_COLOR} where siren = S.siren)
    WHERE siren like '${SIREN_PREFIX}%';
    "

    echo -n "    + numéro de TVA..."
    chronopsql "
    UPDATE unite_legale_${NEW_COLOR} S
    SET numero_tva_intra = (SELECT CASE WHEN tvanumber < 10 THEN concat('FR0',tvanumber,siren) ELSE CONCAT('FR',tvanumber,siren) END FROM (SELECT (12+(3*bigintsiren)%97)%97 as tvanumber, siren FROM (select CAST (siren as BIGINT) as bigintsiren,siren from unite_legale_${NEW_COLOR} WHERE siren = S.siren) tbl) tbl2)
            WHERE siren like '${SIREN_PREFIX}%';
    "

done

