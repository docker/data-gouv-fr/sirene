#!/bin/bash

# URLS :
# - https://files.data.gouv.fr/geo-sirene/last/dep/geo_siret_DEPARTEMENT.csv.gz

# Création Table avec ajout d'une colonne ID systématiquement

# Fichier déjà atendu présent.
FNAME=StockEtablissement_utf8.zip

# On travaille dans le volume persistant
cd /downloads
rm -f *.csv

# On attend que le DL soit fait.
if [ -f "${FNAME/zip/csv}.lock" ]; then
    echo -n "Lock présent, attente du téléchargement"
    while [ -f "${FNAME/zip/csv}.lock" ]; do
        echo -n "." && sleep 2
    done
fi



# Schéma de données etablissement
TABLE_NAME=tmp_etablissement

# Format : https://www.sirene.fr/static-resources/doc/Description%20liste%20sirene-fr.pdf?version=1.32.1
DATATYPES='
siren VARCHAR(9),
nic CHARACTER VARYING,
siret VARCHAR(14),
statut_diffusion CHARACTER VARYING,
date_creation date default NULL,
tranche_effectifs CHARACTER VARYING,
annee_effectifs CHARACTER VARYING,
activite_principale_registre_metiers CHARACTER VARYING,
date_dernier_traitement TIMESTAMP default NULL,
etablissement_siege BOOLEAN,
nombre_periodes SMALLINT,
complement_adresse CHARACTER VARYING,
numero_voie CHARACTER VARYING,
indice_repetition CHARACTER VARYING,
dernier_numero_voie CHARACTER VARYING,
indice_repetition_dernier_numero_voie CHARACTER VARYING,
type_voie CHARACTER VARYING,
libelle_voie CHARACTER VARYING,
code_postal CHARACTER VARYING,
libelle_commune CHARACTER VARYING,
libelle_commune_etranger CHARACTER VARYING,
distribution_speciale CHARACTER VARYING,
code_commune CHARACTER VARYING,
code_cedex CHARACTER VARYING,
libelle_cedex CHARACTER VARYING,
code_pays_etranger CHARACTER VARYING,
libelle_pays_etranger CHARACTER VARYING,
identifiant_adresse  CHARACTER VARYING,
coordonnee_lambert_abscisse  CHARACTER VARYING,
coordonnee_lambert_ordonnee  CHARACTER VARYING,
complement_adresse_2 CHARACTER VARYING,
numero_voie_2 CHARACTER VARYING,
indice_repetition_2 CHARACTER VARYING,
type_voie_2 CHARACTER VARYING,
libelle_voie_2 CHARACTER VARYING,
code_postal_2 CHARACTER VARYING,
libelle_commune_2 CHARACTER VARYING,
libelle_commune_etranger_2 CHARACTER VARYING,
distribution_speciale_2 CHARACTER VARYING,
code_commune_2 CHARACTER VARYING,
code_cedex_2 CHARACTER VARYING,
libelle_cedex_2 CHARACTER VARYING,
code_pays_etranger_2 CHARACTER VARYING,
libelle_pays_etranger_2 CHARACTER VARYING,
date_debut  date default NULL,
etat_administratif CHARACTER VARYING,
enseigne_1 CHARACTER VARYING,
enseigne_2 CHARACTER VARYING,
enseigne_3 CHARACTER VARYING,
denomination_usuelle CHARACTER VARYING,
activite_principale CHARACTER VARYING,
nomenclature_activite_principale CHARACTER VARYING,
caractere_employeur CHARACTER VARYING,
'


SQL="CREATE UNLOGGED TABLE ${TABLE_NAME} (id SERIAL NOT NULL,"
COLS=""

while read COL ; do

    # On ignore les lignes commentées et celles vides
    [ "${COL:0:1}" = "#" ] && continue
    [ -z "${COL}" ] && continue

    COL_NAME=${COL# *} ; COL_NAME=${COL_NAME%% *}
    COL_TYPE=${COL#*${COL_NAME} }

    COLS="${COLS} ${COL_NAME},"
    SQL="${SQL} ${COL_NAME} ${COL_TYPE}"
done <<< ${DATATYPES}

# Ajout des colonnes timestamps (pas concernées dans le CSV)
SQL="${SQL}
date_fin   date default NULL,
created_at TIMESTAMP default (now() at time zone 'utc'),
updated_at TIMESTAMP default (now() at time zone 'utc'),
"

# Retrait de la dernière virgule
SQL="${SQL%,*} );"
COLS="${COLS%,}"

chronopsql "DROP TABLE IF EXISTS ${TABLE_NAME} CASCADE;"
chronopsql "${SQL}"
chronopsql "\d ${TABLE_NAME}"


# Extraction uniquement si on a pas déjà le CSV du même mois.
TS_ZIP=$(date +%Y%m01 --date=@$(stat -c %Y ${FNAME}))

if [ "${DEVMODE:-false}" = false ] \
&& [ -f "${FNAME/zip/csv}" ] &&  TS_CSV=$(date +%Y%m01 --date=@$(stat -c %Y ${FNAME/zip/csv})) \
&& [  "${TS_CSV:-0}" == "${TS_ZIP}"  ] ; then

      echo "${FNAME/zip/csv} déjà extrait et du même mois que le zip."

else

   echo "$0: Extration de ${FNAME}... "
   [ -f ${FNAME} ] && unzip -qc ${FNAME} | head -n 1        >  ${FNAME/zip/csv}
   NF=$( awk -F, '{ print NF}' < ${FNAME/zip/csv})

   if [ "${DEVMODE:-false}" = true ]; then
      FILTRE="${DEVMODE_ET_FILTRE:-56}"
      echo "$0: DEVMODE, filtre sur le département : ${FILTRE}"
      [ -f ${FNAME} ] && unzip -qc ${FNAME} | grep -E ",${FILTRE}[0-9]{3},"  >> ${FNAME/zip/csv}

      cp ${FNAME/zip/csv} ${FNAME/zip/csv}.debug

   else

      [ -f ${FNAME} ] && unzip -qc ${FNAME} | tail -n +2                     >> ${FNAME/zip/csv}

   fi

   echo "$0: Extraction OK."

fi
# On s'assure qu'il y a un line feed au bout (et s'il est déjà là, on pensera à ommettre les lignes vides)
echo "" >> ${FNAME/zip/csv}

# Import dans la DB
echo -n "Import du CSV... ($NF champs) "
# echo COLS=${COLS}

grep -v "^ *$" ${FNAME/zip/csv} | chronopsql "\copy ${TABLE_NAME}(${COLS}) FROM STDIN delimiter ',' CSV HEADER ENCODING 'UTF8';"

# Suppression du CSV
rm -f ${FNAME/zip/csv}

# Clé primaire
echo -n "$0 : Primary key ${TABLE_NAME}(siret)... "
chronopsql "ALTER TABLE ${TABLE_NAME} ADD PRIMARY KEY(siret);"
