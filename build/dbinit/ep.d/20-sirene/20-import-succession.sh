#!/bin/bash

# URL : https://files.data.gouv.fr/insee-sirene/StockEtablissementLiensSuccession_utf8.zip

# Création Table avec ajout d'une colonne ID systématiquement
# Import des données

# Fichier déjà atendu présent.
FNAME=StockEtablissementLiensSuccession_utf8.zip


# On travaille dans le volume persistant
cd /downloads
rm -f *.csv


# On attend que le DL soit fait.
if [ -f "${FNAME/zip/csv}.lock" ]; then
    echo -n "Lock présent, attente du téléchargement"
    while [ -f "${FNAME/zip/csv}.lock" ]; do
        echo -n "." && sleep 2
    done
fi


# Schéma de données
# Succession etablissement
TABLE_NAME=succession_${NEW_COLOR}


# Nom des colonnes est convertit de camelCase à underscore_case
# Ref: https://www.sirene.fr/static-resources/doc/Description%20fichier%20StockEtablissementLiensSuccession.pdf?version=1.23


DATATYPES='
siret VARCHAR(14),
siret_successeur VARCHAR(14),
date TIMESTAMP default NULL,
transfert_siege  BOOLEAN,
continuite_economique  BOOLEAN,
date_dernier_traitement TIMESTAMP default NULL,
'

# SQL="CREATE UNLOGGED TABLE ${TABLE_NAME} (id SERIAL NOT NULL,"
SQL="CREATE TABLE ${TABLE_NAME} (id SERIAL NOT NULL,"
COLS=""

while read COL ; do

    # On ignore les lignes commentées et celles vides
    [ "${COL:0:1}" = "#" ] && continue
    [ -z "${COL}" ] && continue

    COL_NAME=${COL# *} ; COL_NAME=${COL_NAME%% *}
    COL_TYPE=${COL#*${COL_NAME} }

    COLS="${COLS} ${COL_NAME},"
    SQL="${SQL} ${COL_NAME} ${COL_TYPE}"

done <<< ${DATATYPES}

# Ajout des colonnes timestamps (pas concernées dans le CSV)
SQL="${SQL}
created_at TIMESTAMP default (now() at time zone 'utc'),
updated_at TIMESTAMP default (now() at time zone 'utc'),
"

# Retrait de la dernière virgule
SQL="${SQL%,*} );"
COLS="${COLS%,}"

echo "Création de la table"
chronopsql "DROP TABLE IF EXISTS ${TABLE_NAME} CASCADE;"
chronopsql "${SQL}"
chronopsql "\d ${TABLE_NAME}"


# Extraction uniquement si on a pas déjà le CSV du même mois.
TS_ZIP=$(date +%Y%m01 --date=@$(stat -c %Y ${FNAME}))

if [ "${DEVMODE:-false}" = false ] \
&& [ -f "${FNAME/zip/csv}" ] &&  TS_CSV=$(date +%Y%m01 --date=@$(stat -c %Y ${FNAME/zip/csv})) \
&& [  "${TS_CSV:-0}" == "${TS_ZIP}"  ] ; then

      echo "${FNAME/zip/csv} déjà extrait et du même mois que le zip."

else

   echo "$0: Extration de ${FNAME}... "
   [ -f ${FNAME} ] && unzip -qc ${FNAME} | head -n 1        >  ${FNAME/zip/csv}
   NF=$( awk -F, '{ print NF}' < ${FNAME/zip/csv})

   if [ "${DEVMODE:-false}" = true ]; then
      FILTRE="${DEVMODE_SUCCESSION_FILTRE:-202.}"
      echo "$0: DEVMODE, filtre sur les successions de : ${FILTRE}"
      [ -f ${FNAME} ] && unzip -qc ${FNAME} | grep "${FILTRE}" >> ${FNAME/zip/csv}

      cp ${FNAME/zip/csv} ${FNAME/zip/csv}.debug

   else

      [ -f ${FNAME} ] && unzip -qc ${FNAME} | tail -n +2       >> ${FNAME/zip/csv}

   fi

   echo "$0: Extraction OK."

fi
# On s'assure qu'il y a un line feed au bout
echo "" >> ${FNAME/zip/csv}



# Import dans la DB
echo -n "Import du CSV... ($NF champs) "
grep -v "^ *$" ${FNAME/zip/csv}  | chronopsql "\copy ${TABLE_NAME}(${COLS}) FROM STDIN delimiter ',' CSV HEADER ENCODING 'UTF8';"

# Suppression du CSV
rm -f ${FNAME/zip/csv}
