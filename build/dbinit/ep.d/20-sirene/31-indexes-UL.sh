#!/bin/bash

echo -n "$0: Index unite_legale(date_dernier_traitement)... "
chronopsql "CREATE INDEX ${NEW_COLOR}_ul_date_dern_trt ON unite_legale_${NEW_COLOR} (date_dernier_traitement);"

echo -n "$0: Index unite_legale(identifiant_association)... "
chronopsql "CREATE INDEX ${NEW_COLOR}_ul_identifiant_association ON unite_legale_${NEW_COLOR} (identifiant_association);"


# ---------------------
# Ref. 30-import-unite-legale.sh : Tables / vue unite_legale non issues d'une fusion (comme ET) 
# -> donc pas nécessaire de passer par la création de la séquence et de forcer la default sur updated_at
#   -> id déclaré en SERIAL -> séquence créée
#   -> updated_at sur default 'now' = OK
# ---------------------
# echo -n "$0: Sequence unite_legale (id)... "
# chronopsql "DROP SEQUENCE IF EXISTS unite_legale_${NEW_COLOR}_id_seq;"
# chronopsql "CREATE SEQUENCE unite_legale_${NEW_COLOR}_id_seq;"
# chronopsql "SELECT setval('unite_legale_${NEW_COLOR}_id_seq', COALESCE(MAX(id), 0)) FROM unite_legale_${NEW_COLOR};"
# chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ALTER COLUMN id SET NOT NULL;"
# chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ALTER COLUMN id SET DEFAULT nextval('unite_legale_${NEW_COLOR}_id_seq');"

# echo -n "$0: default date unite_legale (updated_at)... "
# chronopsql "ALTER TABLE unite_legale_${NEW_COLOR} ALTER COLUMN updated_at SET DEFAULT (now() at time zone 'utc');"

