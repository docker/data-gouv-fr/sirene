#!/bin/bash

# Création des indexes
# -> en compléments de ceux liés aux primary keys etablissement(siret) et unite_legale(siren)

echo -n "$0: Index etablissement(siren)... "
chronopsql "CREATE INDEX ${NEW_COLOR}_et_siren ON etablissement_${NEW_COLOR} (siren);"


echo -n "$0: Index etablissement(date_dernier_traitement)... "
chronopsql "CREATE INDEX ${NEW_COLOR}_et_date_dern_trt ON etablissement_${NEW_COLOR} (date_dernier_traitement);"


echo -n "$0: Sequence etablissement (id)... "
chronopsql "DROP SEQUENCE IF EXISTS etablissement_${NEW_COLOR}_id_seq;"
chronopsql "CREATE SEQUENCE etablissement_${NEW_COLOR}_id_seq;"
chronopsql "SELECT setval('etablissement_${NEW_COLOR}_id_seq', COALESCE(MAX(id), 0)) FROM etablissement_${NEW_COLOR};"
chronopsql "ALTER TABLE etablissement_${NEW_COLOR} ALTER COLUMN id SET NOT NULL;"
chronopsql "ALTER TABLE etablissement_${NEW_COLOR} ALTER COLUMN id SET DEFAULT nextval('etablissement_${NEW_COLOR}_id_seq');"

echo -n "$0: default date etablissement (updated_at)... "
chronopsql "ALTER TABLE etablissement_${NEW_COLOR} ALTER COLUMN updated_at SET DEFAULT (now() at time zone 'utc');"


#chronopsql "ALTER TABLE your_table ADD CONSTRAINT your_table_id_pk PRIMARY KEY (id);"