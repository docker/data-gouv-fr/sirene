#!/bin/bash

TABLE=etablissement

# echo -n "$0 Passage de la table ${TABLE} en LOGGED:"
# chronopsql "ALTER TABLE ${TABLE}_${NEW_COLOR} SET LOGGED;"

echo -n "$0: Pointage de VUE ${TABLE} sur ${TABLE}_${NEW_COLOR}... "
chronopsql "DROP VIEW IF EXISTS ${TABLE};"
chronopsql "CREATE OR REPLACE VIEW ${TABLE}    AS SELECT * from ${TABLE}_${NEW_COLOR};"

echo -n "$0: Pointage de VUE ${TABLE}_nd sur ${TABLE}_${NEW_COLOR}... "
chronopsql "DROP VIEW IF EXISTS ${TABLE}_nd;"
chronopsql "CREATE OR REPLACE VIEW ${TABLE}_nd AS SELECT * from ${TABLE}_${NEW_COLOR} where statut_diffusion!='O';"

echo -n "$0: Comptage TABLE ${TABLE}_${NEW_COLOR}... "
chronopsql "select count(*) siret from ${TABLE}_${NEW_COLOR};"

echo -n "$0: Présence Vue ${TABLE}"
chronopsql "\dv+ ${TABLE}"

echo -n "$0: Suppression TABLE ${TABLE}_${OLD_COLOR}... "
chronopsql "DROP TABLE IF EXISTS ${TABLE}_${OLD_COLOR};"
