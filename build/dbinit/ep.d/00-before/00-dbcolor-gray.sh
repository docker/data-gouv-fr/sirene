#!/bin/bash


# DBColor
chronopsql "
CREATE OR REPLACE FUNCTION dbcolor()
RETURNS table (
  color text
)
LANGUAGE PLPGSQL
AS \$\$

 BEGIN
    return query
                SELECT 'gray' as color;

END;

\$\$;
"
