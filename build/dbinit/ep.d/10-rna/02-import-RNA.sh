#!/bin/bash
# Traitement des fichiers CSV et import dans la DB

# On travaille dans un volume persistant
cd /downloads
rm -f *.csv

# On attend que le DL soit terminé
while [ -f download.lock ] ; do
    echo "Download lock present, waiting..."
    sleep 5
done

IMPORT_DONE=0
WALDEC_DONE=0

# Qu'importe-t-on ?
if [ "${DEVMODE:-false}" = true ]; then
  # DEVMODE : on ne considère que le 56
  LISTE=${DEVMODE_DEPT_FILTRE:-56}
  echo "DEVMODE : on ne considère que les departements : $LISTE"
  
else
  # PROD-MODE : on importe tout
  LISTE=$(echo {01..19} 2A 2B {21..95} 97{1..8} 98)

fi

echo "Peuplement de la base de données : Import..."
# Import
time for d in $LISTE; do

    # RNA IMPORT
    echo -e "Dep $d : Import ...\c"

    echo $(unzip -j StockAssociations_import.zip "*_dpt_${d}.csv")

    if [ -f *_dpt_${d}.csv ] ; then

      # Fichiers déjà convertis en UTF-8, plus besoin de le faire
      # iconv -f iso8859-1 -t utf8 < *_dpt_${d}.csv | sed 's/;"";/;;/g' > import-utf8.csv
      cat *_dpt_${d}.csv  > import-utf8.csv

      #sed 's/;"";/;;/g'  *_dpt_${d}.csv | head -n 2 > import-utf8.csv
      COLS=$(head -n 1 import-utf8.csv | tr ';' ',')
      # echo $COLS
      chronopsql "\d+ rna_import;"
      < import-utf8.csv   chronopsql "\copy rna_import FROM STDIN delimiter ';' CSV HEADER;"

      rm -f import-utf8.csv *_dpt_${d}.csv
      (( IMPORT_DONE++ ))
    else
      echo "Pas de fichier"
    fi

done


echo "Peuplement de la base de données : Waldec"
time for d in $LISTE; do

    echo -e "Dep $d : Waldec ...\c"
    # FNAME=rna_waldec_${DATE}_dpt_${d}.csv

    echo $(unzip -j StockAssociations_waldec.zip "*_dpt_${d}.csv")

    if [ -f *_dpt_${d}.csv ] ; then

       # Petites transformations :
       # - chaines vides : "" en RIEN -> NULL
       # - chaines numériques entre guillemets (sauf débutant par 0, en valeurs numériques sans guillemets)
       sed -r \
         -e 's/;" *";/;;/g' \
         -e 's/; *;/;;/g' \
         -e 's,\"([1-9][0-9]+)\",\1,g' \
         -e 's/;"0001-01-01";/;;/g' \
         *_dpt_${d}.csv  > waldec-utf8.csv
       COLS=$(head -n 1 waldec-utf8.csv | tr ';' ',' | \
            sed \
             -e 's/adrs_codeinsee,/adresse_code_insee,/'  \
             -e 's/adrs_codepostal,/adresse_code_postal,/'  \
             -e 's/adrs_complement,/adresse_siege,/'  \
             -e 's/adrs_repetition,/adresse_repetition,/'  \
             -e 's/adrs_distrib,/adresse_distribution,/'  \
             -e 's/adrg_achemine,/adresse_gestion_acheminement,/'  \
             -e 's/adrg_codepostal,/adresse_gestion_code_postal,/'  \
             -e 's/adrg_distrib,/adresse_gestion_distribution,/'  \
             -e 's/adrg_complemid,/adresse_gestion_format_postal,/'  \
             -e 's/adrg_complemgeo,/adresse_gestion_geo,/'  \
             -e 's/adrg_libvoie,/adresse_gestion_libelle_voie,/'  \
             -e 's/adrg_declarant,/adresse_gestion_nom,/'  \
             -e 's/adrg_pays,/adresse_gestion_pays,/'  \
             -e 's/adrs_libcommune,/adresse_libelle_commune,/'  \
             -e 's/adrs_libvoie,/adresse_libelle_voie,/'  \
             -e 's/adrs_numvoie,/adresse_numero_voie,/'  \
             -e 's/adrs_typevoie,/adresse_type_voie,/'  \
             -e 's/date_creat,/date_creation,/'  \
             -e 's/date_decla,/date_derniere_declaration,/'  \
             -e 's/date_disso,/date_declaration_dissolution,/'  \
             -e 's/date_publi,/date_publication_creation,/'  \
             -e 's/dir_civilite,/dirigeant_civilite,/'  \
             -e 's/gestion,/code_gestion,/'  \
             -e 's/id,/id_association,/'  \
             -e 's/id_ex,/id_ex_association,/'  \
             -e 's/position,/position_activite,/'  \
             -e 's/publiweb,/autorisation_publication_web,/'  \
             -e 's/rup_mi,/numero_reconnaissance_utilite_publique,/'  \
             -e 's/siteweb,/site_web,/'  \
             -e 's/maj_time/derniere_maj/'  \
            )
       echo OK: $(chronopsql "\copy rna_${NEW_COLOR} FROM 'waldec-utf8.csv' with delimiter ';' CSV HEADER ENCODING 'UTF8';")
       rm -f waldec-utf8.csv *_dpt_${d}.csv
       (( WALDEC_DONE++ ))
    else
       echo "Pas de fichier"
    fi
done


# Ajout colonnes non fournies dans l'import
chronopsql "ALTER TABLE rna_${NEW_COLOR} ADD COLUMN is_waldec varchar(5) default 'true';"


# Fusion  import -> waldec : quelques retraitements de données : il manque des données dans "import"
echo -e "Intégration des données Import -> RNA ...\c"
chronopsql "
insert into rna_${NEW_COLOR}
       select null as id_association, id_ex, siret, rup_mi, gestion,
       date_creat, '1901-01-01' as date_decla, date_publi, '1901-01-01' as date_disso,
       nature, groupement, titre, null as titre_court, objet, objet_social1, objet_social2,
       substr(adr2,42) as adrs_repetition, null as adrs_numvoie, null as adrs_repetition, null as adrs_typevoie, substr(adr1,42) as adrs_libvoie,
       substr(adr3,42) as adrs_distrib, null as adrs_codeinsee, adrs_codepostal, libcom as adrs_libcommune,
       null as adrg_declarant, null as adrg_complemid, null as adrg_complemgeo, null as adrg_libvoie,
       null as adrg_distrib, null as adrg_codepostal, null as adrg_achemine, null as adrg_pays,
       dir_civilite, siteweb, null as publiweb, observation, position, maj_time, 'false' as is_waldec
       from rna_import;
"

echo -e "Destruction de la table rna_import ...\c"
chronopsql "
DROP TABLE rna_import;
"





# Affichages et contrôles
if [ ${IMPORT_DONE} -eq 0 ] || [ ${WALDEC_DONE} -eq 0 ]
then
  echo "*** DONE: IMPORT=${IMPORT_DONE}, WALDEC=${WALDEC_DONE}"
  exit 2
fi

