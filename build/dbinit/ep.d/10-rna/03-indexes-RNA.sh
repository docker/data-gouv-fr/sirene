#!/bin/bash
# Création des indexes

echo "Création des indexes sur rna_${NEW_COLOR}"

# Toutes les associations créées ou ayant déclaré un changement de situation depuis 2009 disposent d’un numéro RNA.
# RNA_waldec : liste des associations disposant d’un numéro RNA.

# RNA -> ID
echo -e "rna_${NEW_COLOR} (id)...\c"
chronopsql "
CREATE INDEX rna_${NEW_COLOR}_id ON rna_${NEW_COLOR} (id_association);
"

# RNA -> Siret
echo -e "rna_${NEW_COLOR} (siret)...\c"
chronopsql "
CREATE INDEX rna_${NEW_COLOR}_siret ON rna_${NEW_COLOR} (siret);
"


