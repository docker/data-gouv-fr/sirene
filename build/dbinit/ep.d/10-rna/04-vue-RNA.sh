#!/bin/bash


# Drop vue RNA
chronopsql "
DROP VIEW IF EXISTS rna;
"




echo "Pointage de VUE rna sur rna_${NEW_COLOR}"
chronopsql "
CREATE OR REPLACE VIEW rna AS SELECT * from rna_${NEW_COLOR};
"


