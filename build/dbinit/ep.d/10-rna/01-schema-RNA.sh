#!/bin/bash
# Création des tables

echo "RNA (waldec), passage en ${NEW_COLOR}"

# Toutes les associations créées ou ayant déclaré un changement de situation depuis 2009 disposent d’un numéro RNA.
# RNA_waldec : liste des associations disposant d’un numéro RNA.

# RNA
chronopsql "
DROP TABLE IF EXISTS rna_${NEW_COLOR};
"

# Create table RNA (waldec)
# id;id_ex;siret;rup_mi;gestion;date_creat;date_decla;date_publi;date_disso;nature;groupement;titre;titre_court;objet;objet_social1;objet_social2;
# adrs_complement;adrs_numvoie;adrs_repetition;adrs_typevoie;adrs_libvoie;adrs_distrib;adrs_codeinsee;adrs_codepostal;adrs_libcommune;
# adrg_declarant;adrg_complemid;adrg_complemgeo;adrg_libvoie;adrg_distrib;adrg_codepostal;adrg_achemine;adrg_pays;
# dir_civilite;siteweb;publiweb;observation;position;maj_time
chronopsql "
CREATE TABLE IF NOT EXISTS rna_${NEW_COLOR} (
     id_association varchar(10),
     id_ex_association varchar(10),
     siret varchar(14),
     numero_reconnaissance_utilite_publique varchar(11) default null,
     code_gestion varchar(4),
     date_creation date,
     date_derniere_declaration date default null,
     date_publication_creation date default null,
     date_declaration_dissolution date default null,
     nature char(1),
     groupement char(1),
     titre varchar(250),
     titre_court varchar(38),
     objet text,
     objet_social1 varchar(6),
     objet_social2 varchar(6),
     adresse_siege varchar(76),
     adresse_numero_voie varchar(10),
     adresse_repetition char(1),
     adresse_type_voie varchar(5),
     adresse_libelle_voie varchar(42),
     adresse_distribution varchar(38),
     adresse_code_insee varchar(5),
     adresse_code_postal varchar(5),
     adresse_libelle_commune varchar(45),
     adresse_gestion_nom varchar(38),
     adresse_gestion_format_postal varchar(38) default null,
     adresse_gestion_geo varchar(38),
     adresse_gestion_libelle_voie varchar(38),
     adresse_gestion_distribution varchar(38),
     adresse_gestion_code_postal varchar(5),
     adresse_gestion_acheminement varchar(32),
     adresse_gestion_pays varchar(38),
     dirigeant_civilite char(2),
     site_web varchar(64),
     autorisation_publication_web char(1),
     observation varchar(255),
     position_activite char(1),
     derniere_maj timestamp
     );
"
    #  is_waldec varchar(5) default 'true',
    #  id serial,
    #  created_at timestamp default (now() at time zone 'utc'),
    #  updated_at timestamp default (now() at time zone 'utc'),
    #  l1_adresse_import varchar(50) default null,
    #  l2_adresse_import varchar(50) default null,
    #  l3_adresse_import varchar(50) default null

# chronopsql "
# CREATE TABLE IF NOT EXISTS rna_${NEW_COLOR} (
#      id_association varchar(10),
#      id_ex_association varchar(10),
#      siret varchar(14),
#      numero_reconnaissance_utilite_publique varchar(11) default null,
#      code_gestion varchar(4),
#      date_creation date,
#      date_derniere_declaration date default null,
#      date_publication_creation date default null,
#      date_declaration_dissolution date default null,
#      nature char(1),
#      groupement char(1),
#      titre varchar(250),
#      titre_court varchar(38),
#      objet text,
#      objet_social1 varchar(6),
#      objet_social2 varchar(6),
#      adresse_siege varchar(76),
#      adresse_numero_voie varchar(10),
#      adresse_repetition char(1),
#      adresse_type_voie varchar(5),
#      adresse_libelle_voie varchar(42),
#      adresse_distribution varchar(38),
#      adresse_code_insee varchar(5),
#      adresse_code_postal varchar(5),
#      adresse_libelle_commune varchar(45),
#      adresse_gestion_nom varchar(38),
#      adresse_gestion_format_postal varchar(38) default null,
#      adresse_gestion_geo varchar(38),
#      adresse_gestion_libelle_voie varchar(38),
#      adresse_gestion_distribution varchar(38),
#      adresse_gestion_code_postal varchar(5),
#      adresse_gestion_acheminement varchar(32),
#      adresse_gestion_pays varchar(38),
#      dirigeant_civilite char(2),
#     #  telephone varchar(10) default null,
#      site_web varchar(64),
#     #  email varchar(64) default null,
#      autorisation_publication_web char(1),
#      observation varchar(255),
#      position_activite char(1),
#      derniere_maj timestamp,
#      is_waldec varchar(5) default 'true',
#      id serial,
#      created_at timestamp default (now() at time zone 'utc'),
#      updated_at timestamp default (now() at time zone 'utc'),
#      l1_adresse_import varchar(50) default null,
#      l2_adresse_import varchar(50) default null,
#      l3_adresse_import varchar(50) default null
#      );
# "



echo "RNA (import)"
# RNA_import : liste des associations sans numéro RNA.

chronopsql "
DROP TABLE IF EXISTS rna_import;
"
# Create table RNA_import (import)
chronopsql "
create UNLOGGED table if not exists rna_import (
    id varchar(14),
    id_ex varchar(10),
    siret varchar(14),
    gestion varchar(4),
    date_creat date,
    date_publi date,
    nature char(1),
    groupement char(1),
    titre varchar(250),
    objet text,
    objet_social1 varchar(6),
    objet_social2 varchar(6),
    adr1 varchar(60),
    adr2 varchar(60),
    adr3 varchar(60),
    adrs_codepostal varchar(5),
    libcom varchar(45),
    dir_civilite char(2),
    siteweb varchar(64),
    observation varchar(128),
    position char(1),
    rup_mi varchar(11),
    maj_time date
    );
"


# chronopsql "
# create UNLOGGED table if not exists rna_import (
#     id varchar(14),
#     id_ex varchar(10),
#     siret varchar(14),
#     gestion varchar(4),
#     date_creat date,
#     date_publi date,
#     nature char(1),
#     groupement char(1),
#     titre varchar(250),
#     objet text,
#     objet_social1 varchar(6),
#     objet_social2 varchar(6),
#     adr1 varchar(60),
#     adr2 varchar(60),
#     adr3 varchar(60),
#     adrs_codepostal varchar(5),
#     libcom varchar(45),
# # adrs_codeinsee varchar(5),
#     dir_civilite char(2),
# # telephone varchar(10) default '0000',
#     siteweb varchar(64),
# # email varchar(64) default 'nomail@nodomain.com',
#     observation varchar(128),
#     position char(1),
#     rup_mi varchar(11),
#     maj_time date
#     );
# "