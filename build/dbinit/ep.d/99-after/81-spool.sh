#!/bin/bash


chronopsql "DROP TABLE IF EXISTS spool;"
chronopsql "CREATE TABLE spool (
                    item_type varchar(32) ,
                    item_id varchar(14),
                    status varchar(24) default NULL,
                    created_at TIMESTAMP default (now() at time zone 'utc'),
                    updated_at TIMESTAMP default NULL,
                    reason varchar(16) default NULL
                );"

chronopsql "ALTER TABLE spool ADD PRIMARY KEY(item_id);"

