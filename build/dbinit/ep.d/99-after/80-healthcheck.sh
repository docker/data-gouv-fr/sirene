#!/bin/bash

# Healthcheck
chronopsql "
CREATE OR REPLACE FUNCTION healthcheck()
RETURNS table (
  message text,
  status text
)
LANGUAGE PLPGSQL
AS \$\$

 BEGIN
    return query
                SELECT
                          'Database is ready.' as message,
                          'healthy' as status
                ;

END;

\$\$;
"

chronopsql "DROP TABLE IF EXISTS status;"
chronopsql "CREATE TABLE status (
                    table_name varchar(32) ,
                    table_rows int,
                    date_dernier_traitement TIMESTAMP default null,
                    last_update TIMESTAMP default null,
                    last_sync   TIMESTAMP default null
                );"

chronopsql "ALTER TABLE status ADD PRIMARY KEY(table_name);"

# Initialisation de la table status
chronopsql "INSERT INTO status (table_name) SELECT table_name from INFORMATION_SCHEMA.views WHERE table_schema = ANY (current_schemas(false))"
