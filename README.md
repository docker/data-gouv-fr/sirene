# API SIRENE


[![pipeline status](https://gitlab.actilis.net/docker-images/data-gouv-fr/sirene/badges/master/pipeline.svg)](https://gitlab.actilis.net/docker-images/data-gouv-fr/sirene/-/commits/master) 


Ce projet permet de construire les images Docker pour déployer une instance de l'API **Sirene** dont la base de données est alimentée par un import des fichiers CSV diffusés l'INSEE.

Ont été ajoutées dans la même base de données les données des associations (**RNA**) publiées par le Ministère de l'Intérieur.


Ce projet ne s'occupe pas du déploiement initial. Se référer au projet **sirene-deploiement** pour cela.


Deux types d'endpoints sont implémentés et sont un sous-ensemble de ce que l'on trouve(ait) à fin 2021 sur :
-  **https://entreprise.data.gouv.fr/api/sirene/v3** (endpoints /etablissements et /unite_legales décommisionnés, mais que ce projet conserve)
-  **https://api.insee.fr/entreprises/sirene/V3/** (endpoints /siret et /siren, plus synthétques)

Pour les associations, les endpoints implémentés sont un sous-ensemble de :
- **https://entreprise.data.gouv.fr/api/rna/v1** (décomissionné, mais conservé ici)

## URLs de base

### Environnements 

Cette API est déployée dans plusieurs environnements :

 - Réféfence : [https://entreprise.data.gouv.fr/api/sirene/v3](https://entreprise.data.gouv.fr) (décomissionné)

 - "prod Actilis" : [https://entreprise.data.actilis.net](https://entreprise.data.actilis.net)

 - ADEME : 
   - pré-intégration "BDD129" : [https://api-entreprise.ademe.intra](https://api-entreprise.ademe.intra)
   - prod-interne "BDD002" : [https://statut-prodsirene.ademe.intra](https://statut-prodsirene.ademe.intra)

 - Applications Hébergeur :
   - [preprod-sirene.ademe-dri.fr](https://preprod-sirene.ademe-dri.fr)
   - [prod-sirene.ademe-dri.fr](https://prod-sirene.ademe-dri.fr)


### APIs

- Sirene : /api/sirene/v3

- RNA : /api/rna/v1



### Swagger

Accès au swagger documentant les endpoints implémentés dans cette version, pour usage par les applications de l'ADEME : **https://api-entreprise.ademe.intra/api/swagger/**

## Endpoints implémentés

### Format de données "entreprise.data.gouv.fr"

#### Endpoint /api/sirene/v3/etablissements/<siret>

Critère de recherche : **un numéro de SIRET**


#### Endpoint /api/sirene/v3/unites_legales/<siren>

Critère de recherche : **un numéro de SIREN**

#### Recherches multi-critères

##### Endpoint /api/sirene/v3/unites_legales

`/sirene/v3/unites_legales/?critère=valeur&autre_critère=autre_valeur`

Critère de recherche disponibles :
- siren
- siret
- identifiant_association
- categorie_juridique

Exemples pour **un identifiant d'association** : `identifiant_association=RNA`

`curl -Ls https://entreprise.data.actilis.net/api/sirene/v3/unites_legales/?identifiant_association=W561012305  | jq -S "."`


##### Endpoint /api/sirene/v3/etablissements

`sirene/v3/etablissements/?critère=valeur&autre_critère=autre_valeur`

Critère de recherche disponibles :
- siren
- siret
- etat_administratif


### Format api.insee.fr

#### Endpoint /sirene/v3/siret/<siret>

Critère de recherche : **un numéro de SIRET**



#### Endpoint /sirene/v3/siren/<siren>

Critère de recherche : **un numéro de SIREN**



## RNA

### Format entreprise.data.gouv.fr

#### Endpoint /api/rna/v1/id/<rna>

Critère de recherche : **un identifiant RNA**

- Exemple : 

   `c-s https://entreprise.data.actilis.net/api/rna/v1/id/W561012305 |jq -S '.'`


#### Endpoint /api/rna/v1/siret/<siret>

Critère de recherche : **un numéro de SIRET**

- Actilis : 

   `c-sL http://entreprise.data.actilis.net/api/rna/v1/siret/42072466800017  | jq -S `



## Healthcheck HTTP

L'un ou l'autre, car c'est la même base :

- /api/sirene/v3/status

- /api/rna/v1/status





# Changelog

- 2025-02-20 : Updater: Traitement de la synchro globale avant le spool.
  > si l'updater a été arrêté un bon moment, c'est impératif pour le maintien de la date de dernier traitement maximale.
  
- 2025-01-16 : DBInit: table etablissement, séquence sur colonne `id` et default value now() sur colonne `updated_at`

- 2024-12-20 : Updater.
  - Updater: Passage sur le nouveau portail INSEE : https://portail-api.insee.fr/.
    -> Deadline MEP : 25/02/2025
    -> Au préalable, créer les comptes sur portail-api + inscription application pour obention api-key

  - Updater: FROM php:8.4-cli-alpine3.21


- 2024-10-10 : Downloader : ne plus télécharger les fichiers Historique par défaut + Fix Oneshot mode.

- 2024-10-09 : Fix: "dbinit" contrôle sur le nombre de champs des CSV retiré.

- 2024-10-03 : Refection dbinit(etablissements) + optimisation updater
  - DBInit: abandon des fichiers geo-siret : on repasse sur StockEtablissements.
  - Updater: temporisations pour éviter les HTTP-429 lors du traitement du spool.


- 2024-09-16 : Typo in /status + Refection readme, déploiement prod-interne

- 2024.07.18 : Add Swagger

- 2024.06.19 : Api-Server - [fix] Verif si clé statut_diffusion est présente avant d'y faire référence.

- 2024.06.13 : Api-Server - Pour les entités à statut_diffusion = "P", on maintient l'affichage des champs à "[ND]".

- 2024.06.08 : Updater : amélioration des logs quand l'updater traite le spool.

- 2024.06.07 : Pour les entités dont statut_diffusion est (devenu) "O" (qui étaient précédemment "P" ou "ND"), on peut avoir des valeurs restées à "ND". Donc :
  - Lors d'une requête retournant un item "ND", :
    - On maquille la réponse pour que les valeurs "[ND]" deviennent **null**.
      [x] getUniteLegale()
      [x] getEtablissement()
      [x] getEtablissements()
      [ ] getEtablissementsByParam()
      [ ] getUnitesLegalesByParam()


    - On insère une demande de raffraichissement dans le spool.

  - Dans l'updater (traitement du spool), dans le cas d'un update pour cause de "Statut Diffusion", on nettoie l'ancienne entréee avant de stocker le résultat du refresh INSEE. Cela permettra d'éliminer les "[ND]" existants et de les remplacer par des **null**.


- 2024-05 : Sirene-Api-Server : les endpoints retournent **null** au lieu de **false** si aucune donnée n'est disponible.

- 2024-04 : Updater  DBInit + API sirene
    - Passage sur version 3.11 de l'API INSEE

    - Ajout du endpoint pour demander la mise à jour dde tous les items depuis une date. Force l'updater à reprendre depuis la date demandée, pour l'item choisi :

      - [PUT] /etablissements/?<query_parameters>
      - [PUT] /unites_legales/?<query_parameters>

      Query_parameters : 
        ddt : annee-mois-jour

- 2024-03 : Updater + API-Server-Sirene : utilisation du spool

    - Si une requête sur un siret ou un siren ramène un enregistrement Non Diffusible :
      - on répond comme d'habitude avec les champs "ND"
      - on alimente le spool pour forcer une tentative de MAJ de la DB au prochain run partiel de l'updater.

    - Lorsqu'on alimente la table de spool, une nouvelle colonne "reason" permet d'indiquer la raison de mise en spool.

    - Passage à PHP 8.3 sur tous les composants concernés.

    - Utilisation de l'api-base implémentant la méthode PUT.

    - Ajout du endpoint pour demander la mise à jour d'un item. Endpoints concernés :

      - [PUT] /etablissements/<id>
      - [PUT] /siret/<id>
      - [PUT] /unites_legales</id>
      - [PUT] /siren/<id>


- 2023-08 (23.08.17) : dbinit + api-server + updater/ implémentation du changement concernant les non diffusibles 
    Voir : https://www.insee.fr/fr/information/6683782
    - la notion n'existe plus, le champ statut_diffusion vaut 'O' ou 'P' (plus de 'N')
    - les données sont juste masquées, mais toute UL et tout ETA est dans la table de base.
    - TODO : supprimer les vues *_nd

    ** le code de contournement concernant OPERAT est conservé ** : supprimer la variable DISABLE_NON_DIFFUSIBLES si on peut désactiver cela.
       --> dans ce cas, on pourra supprimer ce code de contournement.

- 2023-08 : updater/ réglages concernant la date_dernier_traitement, qui n'est pas toujours dispo à la source

- 2023-02 (23.02.17): updater/ gestion du spool de synchro

  Principe du spool :

  - si une requête GET retourne une erreur, on trace dans le spool
  - l'updater parcourt le spool pour vérifier auprès de Insee si on peut synchroniser la donnée
  - TODO (ou pas): toute requête POST sur /spool/siret ou /spool/siren ajoute une entrée dans le spool

- 2023-01 : Updater/ travaille sur les vues directement

- 2023-01 : Revision du status

- 2022-12 : Revision de l'updater (geo-codage)

- 2022-10 : api-gateway, basé sur Alpine 3.16, non-root, port 8000

- 2022-10 : ajout de la propriété "societe_mission".

- 2022-08 : gestion des unités et établissements non-diffusibles
  - ENV-Var DISABLE_NON_DIFFUSIBLES : permet de retourner une 404 pour le sentités non diffusibles (compatibilité Opérat).

- 2022-06 : (dbinit) ajout des unités et établissements non-diffusibles, nouvelles vues associées.


